
import immutables
from pqdict import pqdict

from ast2ba import *
from ast import Type as AstType
from mytypes import *


# =================== Typing functions ==============================

INT_BINOPS = {"+", "-", "/", "*", "%"}
BOOL_BINOPS = {"<", "<=", ">", ">=", "&&", "||", "==", "!=", "and", "or"}

def get_type(ba: BasicAction, typemap: immutables.Map, env: Env):
    """Returns the type of the given expression in the environment given by 
    the provided type map and type-checking environment."""
    if isinstance(ba, BNil): 
        return TYPESET_NIL
    
    elif isinstance(ba, BNum):
        return TYPESET_INT

    elif isinstance(ba, BBool):
        return TYPESET_BOOL
    
    elif isinstance(ba, BPanic):
        return TYPESET_PANIC
    
    elif isinstance(ba, BLet):
        return TYPESET_NIL
    
    elif isinstance(ba, BAssign):
        return TYPESET_NIL
    
    elif isinstance(ba, BBin):
        if ba.op in INT_BINOPS:
            return TYPESET_INT
        elif ba.op in BOOL_BINOPS:
            return TYPESET_BOOL
        else:
            raise Exception("Unhandled binop in 'typ': {}".format(ba.op))
    
    elif isinstance(ba, BUn):
        if ba.op == "!" or ba.op == "not":
            return TYPESET_BOOL
        elif ba.op == "+":
            return TYPESET_INT
        elif ba.op == "-":
            return TYPESET_INT
        else:
            raise Exception("Unhandled unop in 'typ': {}".format(ba.op))
    
    elif isinstance(ba, BVar):
        var = ba.ident
        if not var in typemap:
            raise Exception("Reference to unbound variable '{}' has not been handled by the binder!".format(var))
        else:
            return typemap[var]
    
    elif isinstance(ba, BTypeAssert):
        return ba.type
    
    elif isinstance(ba, BNew):
        try:
            return env.get_type_set(ba.type)
        
        except InvalidType as e:
            return e.type
    
    elif isinstance(ba, BIndexing):
        value_type = get_type(ba.value, typemap, env)
        if value_type.can_be_used_as(TYPESET_VEC):
            item_type = TypeSet(value_type.types[0].generic_params[0])
            return item_type
        else:
            return TYPESET_PANIC
    
    elif isinstance(ba, BCall):
        # Hack something in for builtins:
        funcname = ba.ident.ident
        if funcname in env.funsigs:
            return env.funsigs[funcname].ret_type
        
        # This feels like a hack, but it's probably necessary for builtins
        elif funcname == "vec_push":
            return TYPESET_NIL
        
        elif funcname == "vec_pop":
            if not len(ba.args) >= 1:
                return TYPESET_PANIC
            
            vec_typ = get_type(ba.args[0], typemap, env)
            if not vec_typ.can_be_used_as(TYPESET_VEC):
                return TYPESET_PANIC
            
            return get_item_type(vec_typ)
        
        elif funcname == "vec_len":
            return TYPESET_INT

        else:
            return TYPESET_PANIC
    
    elif isinstance(ba, BMember):
        typ = get_type(ba.expr, typemap, env)
        # TODO: handle unions properly here
        if len(typ.types) > 1:
            return TYPESET_PANIC

        for field, fieldtyp in typ.types[0].signature.fields:
            if field == ba.member:
                return fieldtyp
        
        return TYPESET_PANIC

    else:
        raise Exception("get_type: Unhandled Basic Action: {}".format(type(ba).typename))


INT_INT_OPS = {"+", "-", "/", "*", "%"}
INT_BOOL_OPS = {"<", "<=", ">", ">="}
EQ_BOOL_OPS = {"==", "!="}
BOOL_BOOL_OPS = {"&&", "||", "and", "or"}

# The expression should have the given type, and if it isn't known to always be 
# that, a type-assertion action should be inserted to ensure that it will be 
# valid at run-time.
def type_assert(ba: BasicAction, exp_typ: TypeSet, tm_mm: immutables.Map, 
        env: Env):
    """Ensures that the given basic action has the expected type, or wraps it 
    in a runtime type-assertion to ensure that it will be correct or abort 
    the program. Returns a checked basic action, and whether a variable
    was constrained by the type assertion."""
    # Make sure to first validate the basic action
    ba, var_constrained = validate(ba, tm_mm, env)

    #print("  Asserting that {} should be {}".format(ba, exp_typ))
    typ = get_type(ba, tm_mm, env)

    #print("Gotten type: {}".format(typ))
    if not typ.can_be_used_as(exp_typ):
        #print("    Inserting type-assert.")
        if isinstance(ba, BVar):
            #print("      Restricting var '{}' to {}".format(ba.ident, exp_typ))
            tm_mm[ba.ident] = exp_typ
            var_constrained = True
        return (BTypeAssert(ba, exp_typ, ba.span), var_constrained)
    else:
        return (ba, var_constrained)


def get_item_type(vec_type: TypeSet):
    """Returns the item type of the given vector"""
    return TypeSet(vec_type.types[0].generic_params[0])


def validate_loc(loc: BasicAction, tm_mm: immutables.Map, env: Env):
    if isinstance(loc, BIndexing):
        # Check that the var is a vector
        loc.value, var_constrained = type_assert(loc.value, TYPESET_VEC, tm_mm, env)
        target_type = get_type(loc.value, tm_mm, env)
        if target_type.can_be_used_as(TYPESET_VEC):
            item_type = get_item_type(target_type)
        else:
            item_type = None

        loc.index, upd = type_assert(loc.index, TYPESET_INT, tm_mm, env)
        var_constrained |= upd
        return (loc, item_type, var_constrained)

    elif isinstance(loc, BMember):
        loc.expr, var_constrained = validate(loc.expr, tm_mm, env)
        typ = get_type(loc.expr, tm_mm, env)
        # TODO: handle unions properly here
        if len(typ.types) > 1:
            loc.expr = BPanic("Cannot access member on ambiguous type", loc.expr.span)
            return (loc, None, var_constrained)

        for field, fieldtyp in typ.types[0].signature.fields:
            if field == loc.member:
                return (loc, fieldtyp, var_constrained)
        
        return (BPanic("Field '{}.{}' was not declared".format(typ, loc.member), loc.span), None, var_constrained)
    
    else:
        raise Exception("Unhandled loc type")


def validate(ba: BasicAction, tm_mm: immutables.Map, env: Env):
    """Validates the types inside the given basic action, potentially adding 
    type-assertions as necessary. If these assertions constrain a variable, 
    the given 'typemap mutation manager' will be updated. Returns a
    validated basic action, and whether a variable was constrained by
    a type-assertion."""
    #print("Validating {}...".format(ba))
    var_constrained = False
    if isinstance(ba, BNil): pass
    elif isinstance(ba, BBool): pass
    elif isinstance(ba, BNum): pass
    elif isinstance(ba, BPushScope): pass
    elif isinstance(ba, BPopScope): pass
    elif isinstance(ba, BElse): pass
    elif isinstance(ba, BPanic): pass
    elif isinstance(ba, BBin):
        if ba.op in INT_INT_OPS:
            ba.left, var_constrained = type_assert(ba.left, TYPESET_INT, tm_mm, env)
            ba.right, upd = type_assert(ba.right, TYPESET_INT, tm_mm, env)
            var_constrained |= upd
        
        elif ba.op in INT_BOOL_OPS:
            ba.left, var_constrained = type_assert(ba.left, TYPESET_INT, tm_mm, env)
            ba.right, upd = type_assert(ba.right, TYPESET_INT, tm_mm, env)
            var_constrained |= upd
       
        elif ba.op in EQ_BOOL_OPS:
            tl = get_type(ba.left, tm_mm, env)
            ba.right, var_constrained = type_assert(ba.right, tl, tm_mm, env)
        
        elif ba.op in BOOL_BOOL_OPS:
            ba.left, var_constrained = type_assert(ba.left, TYPESET_BOOL, tm_mm, env)
            ba.right, upd = type_assert(ba.right, TYPESET_BOOL, tm_mm, env)
            var_constrained |= upd

        else:
            raise Exception("validate: Unhandled binop: '{}'".format(ba.op))
            
    elif isinstance(ba, BUn):
        if ba.op == "!" or ba.op == "not":
            ba.value, var_constrained = type_assert(ba.value, TYPESET_BOOL, tm_mm, env)
        elif ba.op == "+" or ba.op == "-":
            ba.value, var_constrained = type_assert(ba.value, TYPESET_INT, tm_mm, env)
        else:
            raise Exception("validate: Unhandled unop: '{}'".format(ba.op))
    
    elif isinstance(ba, BLet):
        if ba.value != None:
            ba.value, var_constrained = validate(ba.value, tm_mm, env)
    
    elif isinstance(ba, BAssign):
        if isinstance(ba.loc, BVar):
            # TODO: check that the var is mutable
            ba.value, var_constrained = validate(ba.value, tm_mm, env)


        elif isinstance(ba.loc, BIndexing):
            ba.loc, typ, var_constrained = validate_loc(ba.loc, tm_mm, env)

            if typ is not None:
                ba.value, upd = type_assert(ba.value, typ, tm_mm, env)
            else:
                ba.value, upd = validate(ba.value, tm_mm, env)
            var_constrained |= upd
        
        elif isinstance(ba.loc, BMember):
            ba.loc, typ, var_constrained = validate_loc(ba.loc, tm_mm, env)
            if typ is not None:
                ba.value, upd = type_assert(ba.value, typ, tm_mm, env)
            else:
                ba.value, upd = validate(ba.value, tm_mm, env)
            var_constrained |= upd
        
        elif isinstance(ba.loc, BPanic):
            # If the location is invalid, validate the value.
            ba.value, var_constrained = validate(ba.value, tm_mm, env)
    
        else:
            raise Exception("validate: Unhandled loc type: '{}'".format(type(ba.loc).typename))
    
    elif isinstance(ba, BTest):
        _, var_constrained = type_assert(ba.value, TYPESET_BOOL, tm_mm, env)
    
    elif isinstance(ba, BVar):
        pass

    elif isinstance(ba, BNew):
        try:
            env.get_type(ba.type)
        except InvalidType as e:
            return (BPanic(e.errors[-1], ba.span), False)
    
    elif isinstance(ba, BIndexing):
        ba, _, var_constrained = validate_loc(ba, tm_mm, env)
    
    elif isinstance(ba, BTypeAssert):
        # This should be right?
        pass

    elif isinstance(ba, BCall):
        #print("funcall ba.ident: {}".format(format_ba(ba.ident)))
        if isinstance(ba.ident, BPanic):
            return (ba, False)
        
        funcname = ba.ident.ident
        if not funcname in env.funsigs:
            # Validate builtins
            if funcname == "vec_push":
                if len(ba.args) != 2:
                    return (BPanic("Function 'vec_push' takes 2 arguments but was given {}".format(len(ba.args)), ba.span), False)
                
                vec_type = get_type(ba.args[0], tm_mm, env)
                if not vec_type.can_be_used_as(TYPESET_VEC):
                    # Add the necessary type-assert
                    ba.args[0], var_constrained = type_assert(ba.args[0], TYPESET_VEC, tm_mm, env)
                    ba.args[1], upd = validate(ba.args[1], tm_mm, env)
                    var_constrained |= upd
                    return (ba, var_constrained)

                ba.args[0], var_constrained = type_assert(ba.args[0], TYPESET_VEC, tm_mm, env)
                item_type = get_item_type(vec_type)
                ba.args[1], upd = type_assert(ba.args[1], item_type, tm_mm, env)
                var_constrained |= upd
                return (ba, var_constrained)

            elif funcname == "vec_pop":
                if len(ba.args) != 1:
                    return (BPanic("Function 'vec_pop' takes 1 argument but was given {}".format(len(ba.args)), ba.span), False)
                
                ba.args[0], var_constrained = type_assert(ba.args[0], TYPESET_VEC, tm_mm, env)
                return (ba, var_constrained)
            
            elif funcname == "vec_len":
                if len(ba.args) != 1:
                    return (BPanic("Function 'vec_pop' takes 1 argument but was given {}".format(len(ba.args)), ba.span), False)
                
                ba.args[0], var_constrained = type_assert(ba.args[0], TYPESET_VEC, tm_mm, env)
                return (ba, var_constrained)


            else:
                return (BPanic("Reference to undefined function '{}'".format(funcname), ba.span), False)
        
        sig = env.funsigs[funcname]
        if len(ba.args) != len(sig.argdefs):
            return (BPanic("Function '{}' takes {} argument{} but was given {}".format(funcname, len(sig.argdefs), "s" if len(sig.argdefs) > 1 else "", len(ba.args)), ba.span), False)

        ck_args = []
        for i, (_, exp_typ) in enumerate(sig.argdefs):
            arg = ba.args[i]
            ck_arg, upd = type_assert(arg, exp_typ, tm_mm, env)
            var_constrained = var_constrained or upd
            ck_args.append(ck_arg)
        
        ba.args = ck_args
    
    elif isinstance(ba, BMember):
        ba, _, var_constrained = validate_loc(ba, tm_mm, env)

    else:
        raise Exception("validate: Unhandled Basic Action type: {}".format(type(ba).typename))
    
    return (ba, var_constrained)
    


def transfer(typemap: immutables.Map, ba: BasicAction, env: Env):
    """Applies the environment change from running the given basic action
    to the given typemap in the specified type-checking environment."""
    if isinstance(ba, BNil): pass
    elif isinstance(ba, BBool): pass
    elif isinstance(ba, BNum): pass
    elif isinstance(ba, BPushScope): pass
    elif isinstance(ba, BPopScope): pass
    elif isinstance(ba, BElse): pass
    elif isinstance(ba, BBin): pass
    elif isinstance(ba, BUn): pass
    elif isinstance(ba, BPanic): pass
    elif isinstance(ba, BLet):
        var = ba.ident
        if ba.value is not None:
            t = get_type(ba.value, typemap, env)
            if not var in typemap:
                #print("    Adding var {} to typemap".format(var))
                return typemap.set(var, t)
            else:
                if typemap[var] != t:
                    typemap = typemap.set(var, t)
                return typemap
        else:
            return typemap.set(var, TYPESET_UNINIT)
    
    elif isinstance(ba, BAssign):
        if isinstance(ba.loc, BVar):
            var = ba.loc.ident
            #loct = typ(ba.loc, typemap)
            t = get_type(ba.value, typemap, env)
            # Ignore unbound vars
            if not var in typemap:
                return typemap
            
            if typemap[var] is not t:
                typemap = typemap.set(var, t)

            return typemap

        else:
            # This cannot change the type of the variable.
            pass
    
    elif isinstance(ba, BTest): pass # Check here?
    elif isinstance(ba, BVar): pass
    elif isinstance(ba, BCall): pass
    elif isinstance(ba, BIndexing): pass
    elif isinstance(ba, BMember): pass

    else:
        raise Exception("Unhandled Basic Action type in 'transfer': {}".format(type(ba).typename))
    
    return typemap


def merge(maps):
    """Merges the given type maps into a single map"""
    if len(maps) == 1:
        return maps[0]
    
    dst = maps[0]
    with dst.mutate() as mm:
        for src in maps[1:]:
            for var, typ in src.items():
                if not var in mm:
                    mm[var] = typ
                else:
                    typs = mm[var]
                    #print("typs: {}".format(typs))
                    if not typs.contains(typ):
                        new_typs = typs.clone()
                        new_typs.add(typ)
                        mm[var] = new_typs
        dst = mm.finish()
    
    return dst
        


def overwrite_mappings(src: immutables.Map, dst: immutables.Map):
    """Overwrites the destination with mappings from the source and returns the resulting source map and whether any mappings were updated."""
    if not dst:
        return (src, bool(src))
    
    updated = False
    new_map = dst
    with dst.mutate() as mm:
        for k, v in src.items():
            if not k in dst:
                #print("    Transferring var '{}'".format(k))
                mm[k] = v
                updated = True
            else:
                if mm[k] != v:
                    #print("    Updating var: '{}'".format(k))
                    mm[k] = v
                    updated = True

        new_map = mm.finish()
    
    if updated:
        return (new_map, True)
    else:
        return (dst, False)


# ==================== Graph Traverser ====================================


def type_check(graph: Prograph, env: Env):
    """Computes the possible types for each variable at each point in the
    given program graph"""
    vert_names = graph.get_vertex_names()

    # Weigh the priority of visiting vertices based on their order in a 
    # BFS-traversal of the program.
    bfs_order = {
        k: int(v) for k, v in vert_names.items() 
        if k != Prograph.START and k != Prograph.END
    }
    bfs_order[Prograph.START] = 0
    bfs_order[Prograph.END] = len(graph.edges)
    
    # The search-type is a DFS where 'else'-branches are taken later
    frontier = pqdict({Prograph.START: bfs_order[Prograph.START]})

    # Keeps track of whether a vertex has been visited
    visited = set()

    # Initialize the domain
    domain = {}

    while frontier:
        v = frontier.pop()
        updated = not v in visited
        visited.add(v)

        typemap = domain.get(v)
        if typemap is None:
            typemap = immutables.Map()
        
        maps = []
        for src, ba in graph.rev.get(v, {}).items():
            if not src in domain:
                continue
            src_map = domain[src]
            with src_map.mutate() as tm_mm:
                # Update the label with type assertions if necessary
                valid_ba, var_constrained = validate(ba, tm_mm, env)
                graph.connect(src, valid_ba, v)
                src_map = tm_mm.finish()
                updated |= var_constrained
            
            maps.append(transfer(src_map, ba, env))
        
        if maps:
            merged = merge(maps)
            typemap, overwritten = overwrite_mappings(merged, typemap)
            updated |= overwritten
        
        if updated:
            domain[v] = typemap
            if not v in graph.edges:
                continue
            
            for n, label in graph.edges[v].items():
                frontier[n] = bfs_order[n]
    
    return domain

            






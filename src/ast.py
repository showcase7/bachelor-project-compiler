# The AST module for the bachelor tan compiler

from lexer import Span


def fmt_fields(fields):
    parts = []
    parts.append("(")
    last = len(fields) - 1
    for i, (field, typ) in enumerate(fields):
        parts.append(field)
        parts.append(": ")
        parts.append(typ.__name__)
        if i != last:
            parts.append(", ")
    parts.append(")")
    return "".join(parts)


class AstNode:
    pass


class Expr(AstNode):
    pass


def ast_node(name, supertype, fields):
    """Creates a new AST node class with the given name, supertype and
    fields"""
    class Node(supertype):
        __name__ = name
        typename = name
        __slots__ = list(map(lambda f: f[0], fields))
        def __init__(self, *args):
            super()
            if len(args) != len(fields):
                raise Exception("Ast node '{}' takes args {}, got {}".format(
                    name, fmt_fields(fields), args
                ))
            
            for i, arg in enumerate(args):
                field, typ = fields[i]
                if not isinstance(arg, typ):
                    raise Exception("Ast node '{}' expected arg '{}' to be of type {}, got '{}' ({})".format(
                        name, field, typ.__name__, arg, type(arg).__name__
                    ))
                setattr(self, field, arg)
        
        def __repr__(self):
            parts = [name]
            parts.append("(")
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                parts.append(field)
                parts.append(": ")
                parts.append(str(getattr(self, field)))
                if i != last:
                    parts.append(", ")
            parts.append(")")
            return "".join(parts)
        
        def __str__(self):
            return repr(self)
        
        def __eq__(self, other):
            #print("Eq called for type: {}".format(name))
            if not isinstance(other, type(self)):
                return False
            
            if not isinstance(other, supertype):
                return False
            
            if type(other).typename != name:
                return False
            
            for field, _ in fields:
                if getattr(self, field) != getattr(other, field):
                    return False
            
            return True
        
        def fmt_into(self, sb, indent):
            sb.append(name)
            sb.append(" {\n")
            indent += 1
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                sb.append(indent * 2 * " ")
                sb.append(field)
                sb.append(": ")
                value = getattr(self, field)
                if isinstance(value, AstNode):
                    value.fmt_into(sb, indent)
                
                elif isinstance(value, list):
                    sb.append("[")
                    if value:
                        sb.append("\n")
                    for item in value:
                        sb.append((indent+1) * 2 * " ")
                        if isinstance(item, AstNode):
                            item.fmt_into(sb, indent+1)
                        else:
                            sb.append(str(item))
                        sb.append(",\n")
                    if value:
                        sb.append(indent * 2 * " ")
                    sb.append("]")
                else:
                    sb.append(str(value))
                
                if i != last:
                    sb.append(",")
                sb.append("\n")
            indent -= 1
            sb.append(indent * 2 * " ")
            sb.append("}")
        
        def fmt(self, indent=0):
            sb = []
            self.fmt_into(sb, indent)
            return "".join(sb)
            
        
    return Node

Let = ast_node("Let", Expr, [
    ("is_mut", bool),
    ("ident", str),
    ("expr", Expr),
    ("span", Span),
])

Op = ast_node("Op", Expr, [
    ("op", str),
    ("args", list),
    ("span", Span),
])

Num = ast_node("Num", Expr, [
    ("value", int),
    ("span", Span),
])

Bool = ast_node("Bool", Expr, [
    ("value", bool),
    ("span", Span),
])

Nil = ast_node("Nil", Expr, [
    ("span", Span),
])

Assign = ast_node("Assign", Expr, [
    ("loc", Expr),
    ("op", str),
    ("value", Expr),
    ("span", Span),
])

Un = ast_node("Un", Expr, [
    ("op", str),
    ("inner", Expr),
    ("span", Span),
])

Panic = ast_node("Panic", Expr, [
    ("message", str),
    ("span", Span)
])

TypeDec = ast_node("TypeDec", AstNode, [
    ("ident", str),
    ("fields", list),
    ("span", Span),
])

FunDec = ast_node("FunDec", AstNode, [
    ("ident", str),
    ("argdefs", list),
    ("ret", object), # None or Type
    ("body", list),
    ("span", Span),
])

If = ast_node("If", Expr, [
    ("cond", Expr),
    ("ifbody", list),
    ("elsebody", list),
    ("span", Span),
])

Var = ast_node("Var", Expr, [
    ("ident", str),
    ("span", Span),
])

Type = ast_node("Type", AstNode, [
    ("name", str),
    ("args", list),
    ("span", Span),
])

New = ast_node("New", Expr, [
    ("type", Type),
    ("assignments", list),
    ("span", Span),
])

Call = ast_node("Call", Expr, [
    ("callee", Expr),
    ("args", list),
    ("span", Span),
])

Member = ast_node("Member", Expr, [
    ("object", Expr),
    ("field", str),
    ("span", Span),
])

For = ast_node("For", Expr, [
    ("var", str),
    ("iterable", object),
    ("body", list),
    ("span", Span),
])

Range = ast_node("Range", AstNode, [
    ("start", Expr),
    ("end", Expr),
    ("span", Span),
])

While = ast_node("While", Expr, [
    ("cond", Expr),
    ("body", list),
    ("span", Span),
])

Do = ast_node("Do", Expr, [
    ("body", list),
    ("span", Span),
])

Indexing = ast_node("Indexing", Expr, [
    ("target", Expr),
    ("index", Expr),
    ("span", Span),
])

Panic = ast_node("Panic", Expr, [
    ("message", str),
    ("span", Span),
])

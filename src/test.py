
import os
import traceback
import subprocess
import tempfile

from lexer import Lexer, TokenKind, RawLexer
from parser import parse
from ast2ba import create_program_graph, format_ba
from binder import bind_var_names
from util import graph_to_png, graph_to_type_change_png
from analysis import type_check
from mytypes import parse_env
from mywarnings import collect_warnings

from pprint import pprint
from collections import namedtuple

Result = namedtuple("Result", ["name", "path", "succesful"])

def print_debug_info(source):
    try:
        lexer = Lexer(source)
        print("Tokens:")
        while True:
            token = lexer.next()
            text = token.text if token.kind != TokenKind.Newline else '\\n'
            print(f"  '{text}' ({token.kind}) [{token.span}]")
            if token.kind == TokenKind.Eof:
                break
        print("")

        ast = parse(source)

        print("Top-level items:")
        for expr in ast:
            print(f"  {expr.fmt(indent=1)}")

        print("")
        #graph = create_program_graph(ast)
        
        #print("Graph Edges:")
        #pprint(graph.edges)

        #dot = graph.to_dot(lambda ba, s, d: format_ba(ba))
        #print("")
        #print("Program graph (DOT):")
        #print(dot)
    except:
        print("ERROR IN GETTING DEBUG INFO:")
        tb = traceback.format_exc()
        print(tb)


Test = namedtuple("Test", [
    "expected_result",
    "expected_warnings",
])

TestWarning = namedtuple("TestWarning", [
    "message",
    "lineno",
])

def get_lineno(byte_index: int, source_text: str):
    lineno = 1
    for i, ch in enumerate(source_text):
        if ch == "\n":
            lineno += 1
        if i == byte_index:
            return lineno

    return lineno

def parse_test(source_text: str):
    """Parses test information from the given source text"""
    lexer = RawLexer(source_text)
    token = lexer.next_token()
    expected_result = None
    expected_warnings = []
    while token.kind != TokenKind.Eof:
        if token.kind == TokenKind.Comment:
            text = token.text[1:].strip()
            if text.startswith("result:"):
                rem = text.split("result:", 1)[1].strip()
                try:
                    expected_result = int(rem)
                except Exception:
                    if rem == "None":
                        token = lexer.next_token()
                        continue
                    
                    print(rem)
                    expected_result = text.split('"')[1]

            elif text.startswith("error: "):
                warn_count = text.count('"') // 2
                splits = text.split('"')
                for i in range(warn_count):
                    index = i*2 + 1
                    message = text.split('"')[index]
                    lineno = get_lineno(token.span.start, source_text)
                    expected_warnings.append(TestWarning(message, lineno))
        
        token = lexer.next_token()
        
    return Test(expected_result, expected_warnings)


def run_test(name, source, img_dir):
    print("")
    print(" {} ".format(name.center(78, "=")))
    print("")
    test = parse_test(source)
    
    try:
        ast = parse(source)
        ast = bind_var_names(ast)
        graph = create_program_graph(ast)
        if not os.path.exists(img_dir):
            os.mkdir(img_dir)
        graph_path = os.path.join(img_dir, name + ".png")
        graph_to_png(graph, graph_path)
        env, errors = parse_env(ast)
        if errors:
            print("Env parsing errors:")
            for err in errors:
                print("  {}".format(err))
        
        domain = type_check(graph, env)
        checked_path = os.path.join(img_dir, name + "_tc.png")
        graph_to_type_change_png(graph, domain, checked_path)

        succeeded = True

        # Check that the expected warnings are found
        warnings = collect_warnings(graph, domain, env)
        exp_warns = test.expected_warnings
        for warning in warnings:
            lineno = get_lineno(warning.span.start, source)
            expected = False
            for i, exp_warn in enumerate(exp_warns):
                if lineno == exp_warn.lineno and warning.message == exp_warn.message:
                    expected = True
                    exp_warns.pop(i)
                    break
            
            if not expected:
                print("Line {}: Unexpected warning: '{}'".format(lineno, warning.message))
                succeeded = False
        
        if exp_warns:
            for exp_warn in exp_warns:
                print("Line {}: Missing expected warning: '{}'".format(exp_warn.lineno, exp_warn.message))

            succeeded = False


        return succeeded
    except:
        print("ERROR:")
        tb = traceback.format_exc()
        print(tb)
        print("")
        print("DEBUG INFO:")
        print_debug_info(source)
        
        return False

EXCLUDED = {"for_int.tan", "union.tan", "vec.tan", "enum_idea.tan"}
IMG_DIR = "graphs"

def main():
    results = {}
    test_count = 0
    for test_folder in sorted(os.listdir("test")):
        #print("Checking folder: {}".format(test_folder))
        if not os.path.isdir(os.path.join("test", test_folder)): continue
        folder_res = []
        for test_file in sorted(os.listdir(os.path.join("test", test_folder))):
            #print("Testing file {}".format(test_file))
            if test_file.startswith("."): continue
            if test_file in EXCLUDED: continue

            test_count += 1
            name = os.path.basename(test_file)
            path = os.path.join("test", test_folder, test_file)
            with open(path, "r") as f:
                text = f.read()
            succesful = run_test(test_folder + "." + name, text, IMG_DIR)
            folder_res.append(Result(name, path, succesful))
        results[test_folder] = folder_res
    
    print("TEST RESULTS:")
    succeeded = 0
    all_succesful = True
    for group, test_results in sorted(results.items()):
        name_printed = False
        group_succesful = True
        for name, path, succesful in test_results:
            if not succesful:
                group_succesful = False
                if not name_printed:
                    print("{}:".format(group))
                    name_printed = True
                print("  {}: Failed".format(name))
            else:
                succeeded += 1
        
        if group_succesful:
            print("{}: Succesful".format(group))
        else:
            all_succesful = False
    
    print("")
    print("{}/{} tests succeeded.".format(succeeded, test_count))
    print("")
    if all_succesful:
        print("All tests completed succesfully!")
    else:
        print("The test failed!")


if __name__ == "__main__":
    main()

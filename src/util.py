# util.py (bachelor)
# 
# Utility functions for creating PNG images of program graphs


import subprocess
import tempfile

from ast2ba import format_ba


def graph_to_png(graph, outfile_path):
    """Converts the given program graph to a PNG and saves it to
    the given path. Requires the 'graphviz' executable to be installed and
    in the system PATH"""
    dot_text = graph.to_dot(lambda ba, s, d: format_ba(ba))
    with tempfile.NamedTemporaryFile(mode='w') as f:
        f.write(dot_text)
        f.flush()
        subprocess.run(["dot", "-Tpng", "-o", outfile_path, f.name])


def graph_to_type_change_png(graph, domain, outfile_path):
    """Converts the given program graph to a PNG, adding type-checker
    change annotation to each edge, and saves it to the given path. 
    Requires the 'graphviz' executable to be installed and in the system 
    PATH"""
    vertex_names = graph.get_vertex_names()
    def fmt(ba, src, dst):
        parts = [format_ba(ba)]
        if src not in domain:
            print("SRC {} not in domain!".format(vertex_names[src]))
            return format_ba(ba)
        
        if dst not in domain:
            print("DST {} not in domain!".format(vertex_names[dst]))
            return format_ba(ba)

        tm_src = domain[src]
        tm_dst = domain[dst]
        changes = []
        for var, styp in tm_src.items():
            if not var in tm_dst:
                raise Exception("Var {} has not been transferred from {} to {}".format(var, vertex_names[src], vertex_names[dst]))
            dtyp = tm_dst[var]
            if dtyp != styp:
                changes.append((var, dtyp))
        
        for var, dtyp in tm_dst.items():
            if not var in tm_src:
                changes.append((var, dtyp))
        
        if changes:
            parts.append(" {")
            last = len(changes) - 1
            for i, (var, typ) in enumerate(changes):
                parts.append("{}: {}".format(var, typ))
                if i != last:
                    parts.append(", ")
            
            parts.append("}")
        
        return "".join(parts)
    
    dot_text = graph.to_dot(fmt)
    with tempfile.NamedTemporaryFile(mode='w') as f:
        f.write(dot_text)
        f.flush()
        subprocess.run(["dot", "-Tpng", "-o", outfile_path, f.name]) 
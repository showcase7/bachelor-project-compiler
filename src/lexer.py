# ==============================================================================
#                               Lexer
# ==============================================================================

class Span:
    """Represents a span of text in a source code text string"""
    __slots__ = ["start", "end"]
    def __init__(self, start, end):
        self.start = start
        self.end = end
    
    def until(self, other_span):
        """Returns a span that is the smallest span that contains both this and the given span"""
        return Span(min(self.start, other_span.start), max(self.end, other_span.end))
    
    def line_info(self, text):
        """Returns information about which line and column number the start of this span corresponds to"""
        lineno = 1
        line_start = 0
        pos = self.start
        for i, ch in enumerate(text):
            if i == pos:
                break
            if ch == '\n':
                lineno += 1
                line_start = i + 1
        col = len(text[line_start:pos]) + 1
        rem_lines = text[line_start:].splitlines()
        line = rem_lines[0] if rem_lines else ""
        return (lineno, col, line)
    
    def __repr__(self):
        return "{}..{}".format(self.start, self.end)
    
    def __str__(self):
        return repr(self)


class Token:
    """Represents a syntactically meaningful token for the Tan language"""
    def __init__(self, kind, text, span):
        self.kind = kind
        self.text = text
        self.span = span
    
    def __str__(self):
        return f"Token(kind: {self.kind}, text: {self.text}, span: {self.span})"
    
    def __repr__(self):
        return str(self)


class TokenKind:
    """Namespace for token types"""
    Ident = "identifier"
    Num = "number"
    Unknown = "unknown"
    Space = "space"
    Newline = "newline"
    Comment = "comment"
    Eof = "end-of-file"


SYMBOLS = {
    "=": "Eq",
    "+": "Plus",
    "-": "Minus",
    "*": "Star",
    "/": "Slash",
    "%": "Percent",
    "+=": "PlusEq",
    "-=": "MinusEq",
    "*=": "StarEq",
    "/=": "SlashEq",
    "%=": "PercentEq",
    "==": "EqEq",
    "!=": "NotEq",
    "<": "Lt",
    "<=": "LtEq",
    ">": "Gt",
    ">=": "GtEq",
    "&&": "And",
    "||": "Or",
    ":": "Colon",
    "->": "Arrow",
    ",": "Comma",
    ".": "Dot",
    "..": "DotDot",
    "[": "BrackOpen",
    "]": "BrackClose",
    "(": "ParOpen",
    ")": "ParClose",
    "!": "Not",
}

MAX_SYMBOL_LEN = max(map(len, SYMBOLS.values()))
SYMBOL_STARTS = set(map(lambda s: s[0], SYMBOLS))

KEYWORDS = {
    "let",
    "mut",
    "fn",
    "for",
    "in",
    "while",
    "if",
    "else",
    "end",
    "do",
    "true",
    "false",
    "nil",
    "return",
    "break",
    "continue",
    "match",
    "type",
    "of",
    "new",
    "where",
    "panic",
}

for sym, name in SYMBOLS.items():
    setattr(TokenKind, name, sym)

for kw in KEYWORDS:
    setattr(TokenKind, kw.capitalize(), kw)

setattr(TokenKind, "True_", "true")
setattr(TokenKind, "False_", "false")

class Ref:
    __slots__ = ["value"]
    def __init__(self):
        self.value = None

def try_read_symbol(text, start, ref):
    sublen = MAX_SYMBOL_LEN
    while sublen > 0:
        s = text[start : start+sublen]
        if s in SYMBOLS:
            ref.value = s
            return True
        sublen -= 1
    return False

def can_start_ident(ch):
    return ch.isalpha() or ch == "_"

def can_continue_ident(ch):
    return ch.isalpha() or ch == "_"

def try_read_ident(text, start, ref):
    if can_start_ident(text[start]):
        i = start + 1
        while i < len(text) and can_continue_ident(text[i]):
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_space(text, start, ref):
    if text[start] == "\n":
        ref.value = "\n"
        return True
    elif text[start].isspace():
        i = start + 1
        while i < len(text) and text[i].isspace():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_comment(text, start, ref):
    if text[start] == "#":
        i = start + 1
        while i < len(text) and text[i] != "\n":
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_number(text, start, ref):
    if text[start].isdigit():
        i = start + 1
        while i < len(text) and text[i].isdigit():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def can_start_token(ch):
    return ch.isalpha() or ch.isspace() or ch == "#" or ch in SYMBOL_STARTS

class RawLexer:
    def __init__(self, text):
        self.text = text
        self.ref = Ref()
        self.start = 0
    
    def send_token(self, kind, text):
        """Emits a token and updates the internal progress of the lexer"""
        end = self.start+len(text)
        token = Token(kind, text, Span(self.start, end))
        self.start = end
        return token
    
    def next_token(self):
        """Returns the next token in the stream for the current source text"""
        if self.start >= len(self.text):
            return self.send_token(TokenKind.Eof, "")
        
        elif try_read_symbol(self.text, self.start, self.ref):
            kind = getattr(TokenKind, SYMBOLS[self.ref.value])
            return self.send_token(kind, self.ref.value)
        
        elif try_read_ident(self.text, self.start, self.ref):
            if self.ref.value in KEYWORDS:
                kind = getattr(TokenKind, self.ref.value.capitalize())
                return self.send_token(kind, self.ref.value)
            elif self.ref.value == "or":
                return self.send_token(TokenKind.Or, self.ref.value)
            elif self.ref.value == "and":
                return self.send_token(TokenKind.And, self.ref.value)
            elif self.ref.value == "not":
                return self.send_token(TokenKind.Not, self.ref.value)
            else:
                return self.send_token(TokenKind.Ident, self.ref.value)
        
        elif try_read_space(self.text, self.start, self.ref):
            if self.ref.value == "\n":
                return self.send_token(TokenKind.Newline, self.ref.value)
            else:
                return self.send_token(TokenKind.Space, self.ref.value)
        
        elif try_read_comment(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Comment, self.ref.value)
        
        elif try_read_number(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Num, self.ref.value)
        
        else:
            i = self.start + 1
            while i < len(self.text) and not can_start_token(self.text[i]):
                i += 1
            return self.send_token(TokenKind.Unknown, self.text[self.start:i])


def should_be_filtered(token):
    return token.kind == TokenKind.Space or token.kind == TokenKind.Comment

class Lexer:
    def __init__(self, text):
        self.raw = RawLexer(text)
        self.prev_start = 0
        self.peek_tok = None
        self.was_newline = False
        self.skip_tok = None
    
    def next(self):
        if self.peek_tok is None:
            token = self.raw.next_token()
            while should_be_filtered(token):
                token = self.raw.next_token()
            self.prev_start = token.span.start
            
        else:
            token = self.peek_tok
            self.peek_tok = None
        
        if token.kind == TokenKind.Newline:
            self.was_newline = True
        else:
            self.was_newline = False
        return token
    
    def peek(self):
        if self.peek_tok != None:
            return self.peek_tok
        else:
            token = self.raw.next_token()
            while should_be_filtered(token):
                token = self.raw.next_token()
            self.prev_start = token.span.start
            self.peek_tok = token
            return self.peek_tok
    
    def try_eat(self, kind, token_out):
        peek = self.peek()
        if peek.kind == kind:
            token_out.text = peek.text
            token_out.kind = peek.kind
            token_out.span = peek.span
            self.next()
            return True
        
        # If we cannot eat an expected newline, but there has just been one
        elif kind == TokenKind.Newline and self.was_newline: # TODO: assign
            self.was_newline = False
            self.skip_tok = peek
            return True
        
        # Skip newlines like whitespace in the lexer, if it is not the expected token.
        elif self.peek().kind == TokenKind.Newline:
            self.next()
            return self.try_eat(kind, token_out)
        
        else:
            return False
    
    def fail_expect(self, expected):
        tok = self.next()
        lineno, col, line = tok.span.line_info(self.raw.text)
        print(f"{lineno}:{col}: {line}\n{lineno}:{col}: Expected {expected}, type declaration or expression, found '{tok.text}'")
        raise Exception("Parse failed")
    
    def expect(self, kind, description):
        if kind == TokenKind.Newline and self.was_newline:
            return self.skip_tok
        
        if self.peek().kind != kind:
            self.fail_expect(description)

        return self.next()


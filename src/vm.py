from ast import ast_node


class VmOp:
    pass


Num = ast_node("Num", VmOp, [
    ("value", int)
])
Add = ast_node("Add", VmOp, [])
Sub = ast_node("Sub", VmOp, [])
Mul = ast_node("Mul", VmOp, [])
Div = ast_node("Div", VmOp, [])
Mod = ast_node("Mod", VmOp, [])
Neg = ast_node("Neg", VmOp, [])
Not = ast_node("Not", VmOp, [])

Eq = ast_node("Eq", VmOp, [])
Neq = ast_node("Neq", VmOp, [])
Lt = ast_node("Lt", VmOp, [])
LtEq = ast_node("LtEq", VmOp, [])
Gt = ast_node("Gt", VmOp, [])
GtEq = ast_node("GtEq", VmOp, [])

Swap = ast_node("Swap", VmOp, [])
Dup = ast_node("Dup", VmOp, [])
Pop = ast_node("Pop", VmOp, [])
PopMany = ast_node("PopMany", VmOp, [
    ("count", int)
])

Load = ast_node("Load", VmOp, [
    ("size", int)
])
Store = ast_node("Store", VmOp, [
    ("size", int)
])

Builtin = ast_node("Builtin", VmOp, [
    ("name", str)
])
Call = ast_node("Call", VmOp, [
    ("arg_size", int), 
    ("addr", int),
])

Bnz = ast_node("Bnz", VmOp, [
    ("addr", int)
])
Bez = ast_node("Bez", VmOp, [
    ("addr", int)
])
Jump = ast_node("Jump", VmOp, [
    ("addr", int)
])

TagVar = ast_node("TagVar", VmOp, [
    ("index", int),
    ("tag", int)
])
UntagVar = ast_node("UntagVar", VmOp, [
    ("index", int),
])

Terminate = ast_node("Terminate", VmOp, [])
Nop = ast_node("Nop", VmOp, [])
IPanic = ast_node("IPanic", VmOp, [
    ("message", str),
])

Alloc = ast_node("Alloc", VmOp, [
    ("size", int)
])


# ast2ba.py
# 
# Module for converting AST nodes into basic actions in preparation
# for conversion to program graphs.

from ast import *
from collections import namedtuple, deque
from lexer import Span

class BasicAction:
    pass

class CompoundValue(BasicAction):
    pass

class AtomicValue(CompoundValue):
    pass

BNum = ast_node("BNum", AtomicValue, [
    ("value", int), 
    ("span", Span)
])
BBool = ast_node("BBool", AtomicValue, [
    ("value", bool), 
    ("span", Span),
])
BNil = ast_node("BNil", AtomicValue, [
    ("span", Span),
])
BVar = ast_node("BVar", AtomicValue, [
    ("ident", str),
    ("span", Span),
])
BMember = ast_node("BMember", AtomicValue, [
    ("expr", AtomicValue), 
    ("member", str), 
    ("span", Span),
])
BUn = ast_node("BUn", CompoundValue, [
    ("op", str),
    ("value", AtomicValue), 
    ("span", Span),
])
BBin = ast_node("BBin", CompoundValue, [
    ("op", str),
    ("left", AtomicValue), 
    ("right", AtomicValue), 
    ("span", Span),
])
BAssign = ast_node("BAssign", BasicAction, [
    ("loc", AtomicValue), 
    ("value", CompoundValue), 
    ("span", Span),
])
BLet = ast_node("BLet", BasicAction, [
    ("is_mut", bool), 
    ("ident", str),
    ("value", object), 
    ("span", Span),
])
BPushScope = ast_node("BPushScope", AtomicValue, [
])
BPopScope = ast_node("BPopScope", AtomicValue, [
])
BTest = ast_node("BTest", AtomicValue, [
    ("value", CompoundValue), 
    ("span", Span),
])
BElse = ast_node("BElse", AtomicValue, [
    ("span", Span),
])
BNew = ast_node("BNew", AtomicValue, [
    ("type", Type), 
    ("span", Span),
])
BCall = ast_node("BCall", AtomicValue, [
    ("ident", AtomicValue), 
    ("args", list), 
    ("span", Span ),
])
BIndexing = ast_node("BIndexing", AtomicValue, [
    ("value", AtomicValue), 
    ("index", AtomicValue), 
    ("span", Span ),
])
BPanic = ast_node("BPanic", AtomicValue, [
    ("message", str), 
    ("span", Span ),
])
BTypeAssert = ast_node("BTypeAssert", AtomicValue, [
    ("value", AtomicValue),
    ("type", object), # TypeSet
    ("span", Span),
])

def format_type_into(typ, seq):
    seq.append(typ.name)
    if typ.args:
        seq.append("[")
        last = len(typ.args) - 1
        for i, arg in enumerate(typ.args):
            format_type_into(arg, seq)
            if i != last:
                seq.append(", ")

        seq.append("]")


def format_ba_into(ba, seq):
    #print("Formatting {}".format(ba))
    if isinstance(ba, BNum):
        seq.append(str(ba.value))
    
    elif isinstance(ba, BBool):
        seq.append(str(ba.value))
    
    elif isinstance(ba, BNil):
        seq.append("Nil")
    
    elif isinstance(ba, BVar):
        seq.append(ba.ident)
    
    elif isinstance(ba, BUn):
        seq.append(ba.op)
        format_ba_into(ba.value, seq)
    
    elif isinstance(ba, BBin):
        format_ba_into(ba.left, seq)
        seq.append(" ")
        seq.append(ba.op)
        seq.append(" ")
        format_ba_into(ba.right, seq)
    
    elif isinstance(ba, BAssign):
        #print("Ident: {}".format(ba.loc))
        format_ba_into(ba.loc, seq)
        seq.append(" = ")
        format_ba_into(ba.value, seq)
    
    elif isinstance(ba, BLet):
        seq.append("let ")
        if ba.is_mut:
            seq.append("mut ")
        seq.append(ba.ident)
        if ba.value is not None:
            seq.append(" = ")
            format_ba_into(ba.value, seq)
    
    elif isinstance(ba, BPushScope):
        seq.append("push-scope")
    
    elif isinstance(ba, BPopScope):
        seq.append("pop-scope")
    
    elif isinstance(ba, BTest):
        seq.append("test ")
        format_ba_into(ba.value, seq)
    
    elif isinstance(ba, BElse):
        seq.append("else")
    
    elif isinstance(ba, BNew):
        seq.append("new ")
        format_type_into(ba.type, seq)
    
    elif isinstance(ba, BCall):
        format_ba_into(ba.ident, seq)
        seq.append("(")
        last = len(ba.args) - 1
        for i, arg in enumerate(ba.args):
            format_ba_into(arg, seq)
            if i != last:
                seq.append(", ")
        seq.append(")")

    elif isinstance(ba, BIndexing):
        format_ba_into(ba.value, seq)
        seq.append("[")
        format_ba_into(ba.index, seq)
        seq.append("]")
    
    elif isinstance(ba, BMember):
        format_ba_into(ba.expr, seq)
        seq.append(".")
        seq.append(ba.member)
    
    elif isinstance(ba, BPanic):
        seq.append("panic('")
        seq.append(ba.message)
        seq.append("')")
    
    elif isinstance(ba, BTypeAssert):
        seq.append("(assert ")
        format_ba_into(ba.value, seq)
        seq.append(": ")
        seq.append("{})".format(ba.type))

    else:
        raise Exception("Unhandled Basic Action type in format_ba_into: {}".format(type(ba).__name__))


def format_ba(ba):
    seq = []
    format_ba_into(ba, seq)
    #print(seq)
    return "".join(seq)


IDENT_NO = 0
def gen_ident():
    global IDENT_NO
    tmp = IDENT_NO
    IDENT_NO += 1
    return "$var{}".format(tmp)






class Prograph:
    """A program graph representing a program"""
    START = 0
    END = 1

    def __init__(self):
        self.edges = {}
        self.rev = {}
        self.next_vertex_id = 2
    
    def new_vertex(self):
        """Generates a new vertex"""
        tmp = self.next_vertex_id
        self.next_vertex_id += 1
        return tmp
    
    def connect(self, src, label, dst):
        """Connects the given vertices using the given edge label"""
        #print("  Connecting {} -> {}".format(src, dst))
        if not isinstance(src, int):
            raise Exception("src is not a vertex!")
        if not isinstance(dst, int):
            raise Exception("dst is not a vertex!")

        if src not in self.edges:
            self.edges[src] = {}
        self.edges[src][dst] = label
        if dst not in self.rev:
            self.rev[dst] = {}
        self.rev[dst][src] = label
    
    def connect_new(self, src, label):
        """Adds a connection from the given source vertex to a new vertex, 
        with the given label and returns the new vertex"""
        dst = self.new_vertex()
        self.connect(src, label, dst)
        return dst
    
    def disconnect(self, src, dst):
        """Disconnects the given vertices"""
        label = self.edges[src].pop(dst)
        self.rev[dst].pop(src)
        if len(self.edges[src]) == 0:
            self.edges.pop(src)
        return label
    
    def connect_nil(self, src, dst):
        """Adds a 'nop' connection between the given vertices"""
        self.connect(src, BNil(Span(0, 0)), dst)
    
    def prepend(self, dst, label):
        """Inserts a new vertex with an edge to the given one with the given label, moving all incoming edges to dst to the new vertex, so that the new edge is always taken before reaching the given vertex."""
        incoming = list(self.rev[dst].keys())
        new = self.new_vertex()
        for inc in incoming:
            lab = self.disconnect(inc, dst)
            self.connect(inc, lab, new)
        self.connect(new, label, dst)
    
    def merge(self, src, dst):
        """Merges the first vertex into the second. They should be connected
        before this is called"""
        if src == Prograph.START:
            return
        
        if src in self.rev:
            incoming = list(self.rev[src].keys())
            for inc in incoming:
                #print("  Fixing link: {}->{} => {}->{}".format(inc, src, inc, dst))
                label = self.disconnect(inc, src)
                self.connect(inc, label, dst)
        
        if src in self.edges:
            for dst, label in self.edges[src].items():
                self.rev[dst].pop(src)
            self.edges.pop(src)
    
    def get_vertex_names(self):
        """Returns a name map for the vertices of this graph based on their
        order in a BFS traversal of the graph"""
        next_name = 0
        new_names = {0:"start", 1: "end"}
        frontier = deque()
        frontier.appendleft(0)
        while frontier:
            src = frontier.pop()
            if not src in new_names:
                new_names[src] = str(next_name)
                next_name += 1
            
            if src not in self.edges:
                raise Exception("The graph is disjoint: No further connections from vertex: {}".format(src))

            for dst in sorted(self.edges[src]):
                if not dst in new_names:
                    frontier.appendleft(dst)
        
        return new_names

    
    def to_dot(self, fn_fmt_label):
        """Returns a text string with the graph in the 'graphviz' format"""
        vertex_names = self.get_vertex_names()
        l = [] # lines
        l.append("digraph {")
        l.append('  "_" [shape=none] [label=""];')
        l.append('  "0" [label="start"];')
        l.append('  "1" [shape=doublecircle, label="end"];')
        for src in sorted(self.edges):
            if src == Prograph.START:
                continue
            l.append('  "{}" [label = "{}"];'.format(src, vertex_names[src]))
        
        l.append('  "_" -> "0";')
        for src in sorted(self.edges):
            for dst, label in sorted(self.edges[src].items()):
                l.append('  "{}" -> "{}" [label = " {} "];'.format(src, dst, fn_fmt_label(label, src, dst)))
        l.append("}")
        return "\n".join(l)


ASSIGN_OPS = {
    "+=": "+",
    "-=": "-",
    "*=": "*",
    "/=": "/",
    "%=": "%",
}

# Should this always return a basic value?
# Only if it isn't toplevel.
def connect(expr, graph, start, end, toplevel=False):
    """Connects an expression to the given program graph, by splitting it into basic actions and connecting these to the graph as needed. A basic value describing the value of the expr will be returned."""
    if isinstance(expr, Num):
        #print("connect(Num, {}->{})".format(start, end))
        graph.connect_nil(start, end)
        return BNum(expr.value, expr.span)
    
    elif isinstance(expr, Bool):
        #print("connect(Bool, {}->{})".format(start, end))
        graph.connect_nil(start, end)
        return BBool(expr.value, expr.span)
    
    elif isinstance(expr, Nil):
        #print("connect(Nil, {}->{})".format(start, end))
        graph.connect_nil(start, end)
        return BNil(expr.span)
    
    elif isinstance(expr, Var):
        #print("connect(Var, {}->{})".format(start, end))
        #print("Var")
        graph.connect_nil(start, end)
        return BVar(expr.ident, expr.span)
    
    elif isinstance(expr, Op):
        #print("connect(Op, {}->{})".format(start, end))
        # Unary
        if len(expr.args) == 1:
            node = graph.new_vertex()
            ref = connect(expr.args[0], graph, start, node)
            value = BUn(expr.op, ref, expr.span)
            if not toplevel:
                var = gen_ident()
                let = BLet(False, var, value, expr.span)
                graph.connect(node, let, end)
                return BVar(var, expr.span)
            else:
                graph.connect_nil(node, end)
                return value

        else:
            left_end = graph.new_vertex()
            left = connect(expr.args[0], graph, start, left_end)
            right_end = graph.new_vertex()
            right = connect(expr.args[1], graph, left_end, right_end)
            value = BBin(expr.op, left, right, expr.span)
            if not toplevel:
                var = gen_ident()
                let = BLet(False, var, value, expr.span)
                graph.connect(right_end, let, end)
                return BVar(var, expr.span)
            else:
                graph.connect_nil(right_end, end)
                return value
    
    elif isinstance(expr, Assign):
        #print("connect(Assign, {}->{})".format(start, end))
        
        if expr.op != "=": # Rewrite and call again
            op = ASSIGN_OPS[expr.op]
            value_ast = Op(op, [expr.loc, expr.value], expr.span)
            expr_ast = Assign(expr.loc, "=", value_ast, expr.span)
            connect(expr_ast, graph, start, end)
            return BNil(expr.span)

        else:
            loc_end = graph.new_vertex()
            loc = connect(expr.loc, graph, start, loc_end, toplevel=True)
            value_end = graph.new_vertex()
            value = connect(expr.value, graph, loc_end, value_end, toplevel=True)
            ass = BAssign(loc, value, expr.span)
            graph.connect(value_end, ass, end)
            return BNil(expr.span)
    
    elif isinstance(expr, Let):
        #print("connect(Let, {}->{})".format(start, end))
        value_end = graph.new_vertex()
        value = connect(expr.expr, graph, start, value_end, toplevel=True)
        let = BLet(expr.is_mut, expr.ident, value, expr.span)
        graph.connect(value_end, let, end)
        return BNil(expr.span)
    
    elif isinstance(expr, Do):
        #print("connect(Do, {}->{})".format(start, end))
        # Prepare a return slot
        ret = gen_ident()
        # I should allow non-value lets.
        ret_def = BLet(False, ret, None, expr.span)
        ret_end = graph.connect_new(start, ret_def)

        # Push scope
        push_end = graph.connect_new(ret_end, BPushScope())

        # Body
        body_end = graph.new_vertex()
        value = connect_exprs(expr.body, graph, push_end, body_end)

        # Assign the last value to the return slot
        ret_ass = BAssign(BVar(ret, expr.span), value, expr.span)
        ass_end = graph.connect_new(body_end, ret_ass)

        # Pop scope
        graph.connect(ass_end, BPopScope(), end)
        return BVar(ret, expr.span)
    
    elif isinstance(expr, While):
        #print("connect(While, {}->{})".format(start, end))

        # Evaluate condition
        cond_end = graph.new_vertex()
        cond = connect(expr.cond, graph, start, cond_end)
        test = BTest(cond, cond.span)
        test_end = graph.connect_new(cond_end, test)

        # While body
        body_push_end = graph.connect_new(test_end, BPushScope())

        while_end = graph.new_vertex()
        last = connect_exprs(expr.body, graph, body_push_end, while_end)
        body_end = graph.new_vertex()
        graph.connect(while_end, last, body_end)

        graph.connect(body_end, BPopScope(), start)

        # The else span
        else_end = graph.connect(cond_end, BElse(cond.span), end)

        return BNil(expr.span)
    
    elif isinstance(expr, If):
        #print("connect(If, {}->{})".format(start, end))
        # Prepare return slot
        ret = gen_ident()
        ret_def = BLet(False, ret, None, expr.span)
        ret_end = graph.connect_new(start, ret_def)

        # Push scope
        push_end = graph.connect_new(ret_end, BPushScope())

        # Evaluate condition
        cond_end = graph.new_vertex()
        cond = connect(expr.cond, graph, push_end, cond_end)
        test = BTest(cond, cond.span)
        test_end = graph.connect_new(cond_end, test)
        else_end = graph.connect_new(cond_end, BElse(cond.span))
        expr_end = graph.new_vertex()

        # If-body
        if_end = graph.new_vertex()
        if_value = connect_exprs(expr.ifbody, graph, test_end, if_end)

        ret_ass = BAssign(BVar(ret, expr.span), if_value, expr.span)
        graph.connect(if_end, ret_ass, expr_end)

        # Else-body
        elsebody_end = graph.new_vertex()
        else_value = connect_exprs(expr.elsebody, graph, else_end, elsebody_end)

        else_ret = BAssign(BVar(ret, expr.span), else_value, expr.span)
        graph.connect(elsebody_end, else_ret, expr_end)

        # Pop scope
        graph.connect(expr_end, BPopScope(), end)

        return BVar(ret, expr.span)
    
    elif isinstance(expr, For):
        #print("connect(For, {}->{})".format(start, end))
        if isinstance(expr.iterable, Range):
            
            end_var = gen_ident()
            def_var = Let(True, expr.var, expr.iterable.start, expr.iterable.span)
            def_end = Let(False, end_var, expr.iterable.end, expr.iterable.span)
            def_cond = Op("<", [Var(expr.var, expr.span), Var(end_var, expr.span)], expr.span)
            def_update = Assign(
                Var(expr.var, expr.span), "=", 
                Op("+", [
                    Var(expr.var, expr.span), 
                    Num(1, expr.span)
                ], expr.span), 
                expr.span)
            body = []
            body.append(def_var)
            body.append(def_end)

            whilebody = []
            whilebody.extend(expr.body)
            whilebody.append(def_update)
            def_while = While(def_cond, whilebody, expr.span)

            body.append(def_while)
            ast = Do(body, expr.span)
            #print("Connecting 'for'...")
            return connect(ast, graph, start, end)

        else:
            raise Exception("For-vec not yet supported")
    
    elif isinstance(expr, New):
        #print("connect(New, {}->{})".format(start, end))
        new = BNew(expr.type, expr.type.span)
        new_var = gen_ident()
        let = BLet(True, new_var, new, expr.span)
        if expr.assignments:
            end_new = graph.connect_new(start, let)
            assignments = []
            for field, value in expr.assignments:
                # This might be an error if these are parsed like assignments
                # as it assumes that they are always to a variable.
                lhs = Member(Var(new_var, expr.span), field, value.span)
                assignment = Assign(lhs, "=", value, value.span)
                assignments.append(assignment)
            
            end_push = graph.connect_new(end_new, BPushScope())
            end_body = graph.new_vertex()
            connect_exprs(assignments, graph, end_push, end_body)
            graph.connect(end_body, BPopScope(), end)
        else:
            graph.connect(start, let, end)
        
        return BVar(new_var, expr.span)
    
    elif isinstance(expr, Call):
        #print("connect(Call, {}->{})".format(start, end))
        end_callee = graph.new_vertex()
        callee = connect(expr.callee, graph, start, end_callee)
        end_push = graph.connect_new(end_callee, BPushScope())
        arg_vars = []
        prev = end_push
        next_v = graph.new_vertex()
        for arg in expr.args:
            arg_var = gen_ident()
            set_arg = Let(False, arg_var, arg, arg.span)
            value = connect(set_arg, graph, prev, next_v, True)
            arg_vars.append(BVar(arg_var, arg.span))
            prev = next_v
            next_v = graph.new_vertex()

        graph.connect(prev, BPopScope(), end)

        return BCall(callee, arg_vars, expr.span)
    
    elif isinstance(expr, Indexing):
        #print("connect(Indexing, {}->{})".format(start, end))
        value_end = graph.new_vertex()
        value = connect(expr.target, graph, start, value_end)
        if toplevel:
            index = connect(expr.index, graph, value_end, end)
            return BIndexing(value, index, expr.span)
        else:
            index_end = graph.new_vertex()
            index = connect(expr.index, graph, value_end, index_end)
            let_var = gen_ident()
            let = BLet(False, let_var, BIndexing(value, index, expr.span), expr.span)
            graph.connect(index_end, let, end)
            return BVar(let_var, expr.span)
    
    elif isinstance(expr, Member):
        #print("connect(Member, {}->{})".format(start, end))
        value = connect(expr.object, graph, start, end)
        return BMember(value, expr.field, expr.span)
    
    elif isinstance(expr, Panic):
        #print("connect(Panic, {}->{})".format(start, end))
        graph.connect_nil(start, end)
        return BPanic(expr.message, expr.span)
    
    elif isinstance(expr, TypeDec):
        #print("connect(TypeDec, {}->{})".format(start, end))
        #print("TypeDecs should be part of the env, not the program graph")
        graph.connect_nil(start, end)
        return BNil(expr.span)
    
    elif isinstance(expr, FunDec):
        #print("connect(FunDec, {}->{})".format(start, end))
        #print("FunDecs should be parts of the env, not the program graph")
        graph.connect_nil(start, end)
        return BNil(expr.span)
    
    else:
        raise Exception("Unhandled expr type: {}".format(type(expr).typename))


def connect_exprs(exprs, graph, start, end):
    """Converts a sequence of expressions into basic actions and connects
    them to the given graph between the given start and end vertices"""
    last = len(exprs) - 1
    prev = start
    for i, subexpr in enumerate(exprs):
        if i != last:
            sub_end = graph.new_vertex()
            value = connect(subexpr, graph, prev, sub_end, toplevel=True)
            prev = graph.connect_new(sub_end, value)
        else:
            value = connect(subexpr, graph, prev, end, toplevel=True)
            return value
    
    graph.connect_nil(start, end)
    return BNil(Span(0, 0))


def simplify(graph):
    """Merges the nop-edges of the given graph together so that it becomes 
    smaller"""
    to_be_merged = []
    for src in graph.edges:
        edges = graph.edges[src]
        remove = False
        if len(edges) == 1:
            #print("Found src with edge count 1!")
            for dst, label in edges.items():
                #print("  Label: {}".format(label))
                if isinstance(label, BNil):
                    #print("    Nil!")
                    to_be_merged.append((src, dst))

    redirect = {}
    def get_redirect(vertex):
        while redirect[vertex] != vertex:
            vertex = redirect[vertex]
        return vertex

    for src, dst in to_be_merged:
        if not src in redirect:
            redirect[src] = src
        if not dst in redirect:
            redirect[dst] = dst
        new_src = get_redirect(src)
        new_dst = get_redirect(dst)
        #print("Merging {} with {} = {}->{}".format(src, dst, new_src, new_dst))
        graph.merge(new_src, new_dst)
        redirect[src] = new_dst
        redirect[new_src] = new_dst


def create_program_graph(exprs):
    """Creates a program graph representing the given AST"""
    #print("Creating program graph...")
    graph = Prograph()
    exprs_end = graph.new_vertex()
    last = connect_exprs(exprs, graph, Prograph.START, exprs_end)
    graph.connect(exprs_end, last, Prograph.END)
    simplify(graph)

    return graph

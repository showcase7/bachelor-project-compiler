
import immutables
from ast2ba import *
from analysis import get_type, TypeSet
from mytypes import Env
from lexer import Span
from ast import ast_node


Warning = ast_node("Warning", object, [
    ("message", str),
    ("span", Span),
])


def gen_invalid_type_warning(expected: TypeSet, actual: TypeSet, span: Span):
    return Warning("Expected type {}, got {}".format(expected, actual), span)


def gen_type_assert_warning(expected: TypeSet, actual: TypeSet, span: Span):
    return Warning("Expression of type {} might not always be of type {}".format(actual, expected), span)


def collect_ba_warnings_into(ba: BasicAction, typemap: immutables.Map, 
        env: Env, warnings: list):
    """Traverses the given basic action looking for panics and type assertions to report"""
    if isinstance(ba, BNum): return
    elif isinstance(ba, BBool): return
    elif isinstance(ba, BNil): return
    elif isinstance(ba, BPushScope): return
    elif isinstance(ba, BPopScope): return
    elif isinstance(ba, BVar): return
    elif isinstance(ba, BElse): return
    elif isinstance(ba, BNew): return
    elif isinstance(ba, BMember):
        collect_ba_warnings_into(ba.expr, typemap, env, warnings)
    
    elif isinstance(ba, BUn):
        collect_ba_warnings_into(ba.value, typemap, env, warnings)
    
    elif isinstance(ba, BBin):
        collect_ba_warnings_into(ba.left, typemap, env, warnings)
        collect_ba_warnings_into(ba.right, typemap, env, warnings)
    
    # I should add the warning about assigning a new type to a variable
    elif isinstance(ba, BAssign):
        collect_ba_warnings_into(ba.loc, typemap, env, warnings)
        collect_ba_warnings_into(ba.value, typemap, env, warnings)
    
    elif isinstance(ba, BLet):
        if ba.value is not None:
            collect_ba_warnings_into(ba.value, typemap, env, warnings)
    
    elif isinstance(ba, BTest):
        collect_ba_warnings_into(ba.value, typemap, env, warnings)
    
    elif isinstance(ba, BCall):
        collect_ba_warnings_into(ba.ident, typemap, env, warnings)
        for arg in ba.args:
            collect_ba_warnings_into(arg, typemap, env, warnings)
    
    elif isinstance(ba, BIndexing):
        collect_ba_warnings_into(ba.value, typemap, env, warnings)
        collect_ba_warnings_into(ba.index, typemap, env, warnings)
    
    elif isinstance(ba, BPanic):
        warnings.append(Warning(ba.message, ba.span))
    
    elif isinstance(ba, BTypeAssert):
        typ = get_type(ba.value, typemap, env)
        if not typ.contains(ba.type):
            # I could technically replace it with a panic here
            warnings.append(gen_invalid_type_warning(ba.type, typ, ba.span))
        else:
            # warn
            warnings.append(gen_type_assert_warning(ba.type, typ, ba.span))
    
    else:
        raise Exception("collect_ba_warnings_into: Unsupported basic action: {}".format(type(ba).typename))
    

def collect_warnings(tc_graph: Prograph, domain: dict, env: Env):
    """Collects warnings about panics and type-assertions in the given
    type-checked program graph."""
    warnings = []
    for src, edges in tc_graph.edges.items():
        for dst, label in edges.items():
            collect_ba_warnings_into(label, domain[src], env, warnings)
    
    return warnings


def get_line_pos(byte_index: int, source_text: str):
    if byte_index == 0:
        return (1, 1)
    
    lineno = 1
    line_start = 0
    i = 0
    for i, ch in enumerate(source_text):
        if ch == "\n":
            lineno += 1
            line_start = i+1
        
        if i == byte_index:
            col = i - line_start + 1
            return (lineno, col)
    
    col = i - line_start + 1
    return (lineno, col)



def format_warning(warning: Warning, source_text: str):
    """Formats a warning relating to the given source text for being shown 
    to a user"""
    sl, sc = get_line_pos(warning.span.start, source_text)
    parts = []
    lines = source_text.splitlines()
    lineno_len = len(str(sl+1))

    def fmt_line(lineno):
        line = lines[lineno-1]
        lineno = str(lineno).rjust(lineno_len)
        parts.append(lineno)
        parts.append(": ")
        parts.append(line)

    prevprev = sl - 2
    if prevprev > 0:
        fmt_line(prevprev)
        parts.append("\n")

    prev = sl - 1
    if prev > 0:
        fmt_line(prev)
        parts.append("\n")
    
    fmt_line(sl)
    parts.append("\n")

    parts.append(lineno_len * " ")
    parts.append("  ")
    #parts.append((lineno_len + 2) * "~")
    if sc != 1:
        wave_count = sc - 1
        parts.append(wave_count * "~")
    parts.append("^\n")

    #nextl = sl + 1
    #if nextl < len(lines):
    #    fmt_line(nextl)
    #    parts.append("\n")
    
    parts.append("{}".format(warning.message))

    return "".join(parts)

    

        







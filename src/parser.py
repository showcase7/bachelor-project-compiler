# The parsing module for the bachelor tan compiler

from lexer import Token, TokenKind, Ref, Span, Lexer
from ast import *
from collections import deque

BINOPS = {
    "+", "-", "*", "/", "%",
    "==", "!=", "<", "<=", ">", ">=", 
    "||", "&&",
}

def is_binop(token):
    return token.text in BINOPS

PRECEDENCE = {
    "*": 3,
    "/": 3,
    "%": 3,
    "+": 4,
    "-": 4,
    "<": 6,
    "<=": 6,
    ">": 6,
    ">=": 6,
    "==": 7,
    "!=": 7,
    "&&": 11,
    "and": 11,
    "||": 12,
    "or": 12,
}

def order_binary_expr(stack):
    expr_stack = []
    op_stack = []
    
    def reduce_two():
        op = op_stack.pop()
        right = expr_stack.pop()
        left = expr_stack.pop()
        span = left.span.until(right.span)
        expr_stack.append(Op(op, [left, right], span))
    
    exprs = deque()
    ops = []
    for i in range(0, len(stack)):
        if i % 2 == 0:
            exprs.append(stack[i])
        else:
            ops.append(stack[i])
    
    expr_stack.append(exprs.popleft())
    for op in ops:
        prec = PRECEDENCE[op]
        while op_stack:
            prev_prec = PRECEDENCE[op_stack[-1]]
            if prev_prec < prec:
                reduce_two()
            else:
                break
        
        expr_stack.append(exprs.popleft())
        op_stack.append(op)
    
    while op_stack:
        reduce_two()
    
    return expr_stack.pop()

# ==============================================================================
#                               Parser
# ==============================================================================


ref = Ref()
token = Token(TokenKind.Eof, "", Span(0, 0))


def parse_if(lexer, ref):
    if not lexer.try_eat(TokenKind.If, token):
        return False
    
    start = token.span
    if not parse_expr(lexer, ref):
        lexer.fail_expect("condition expression after 'if'")
    cond = ref.value
    lexer.expect(TokenKind.Do, "'do' after condition")
    ifbody = parse_exprs(lexer)
    if lexer.try_eat(TokenKind.Else, token):
        elsebody = parse_exprs(lexer)
    else:
        elsebody = []
    end = lexer.expect(TokenKind.End, "'end' to end 'if'-statement")
    span = start.until(end.span)
    ref.value = If(cond, ifbody, elsebody, span)
    return True


def parse_num(lexer, ref):
    if lexer.try_eat(TokenKind.Num, token):
        ref.value = Num(int(token.text), token.span)
        return True
    else:
        return False


def parse_bool(lexer, ref):
    if lexer.try_eat(TokenKind.True_, token):
        ref.value = Bool(True, token.span)
        return True
    elif lexer.try_eat(TokenKind.False_, token):
        ref.value = Bool(False, token.span)
        return True
    else:
        return False


def parse_var(lexer, ref):
    if lexer.try_eat(TokenKind.Ident, token):
        ref.value = Var(token.text, token.span)
        return True
    else:
        return False


def parse_let(lexer, ref):
    if not lexer.try_eat(TokenKind.Let, token):
        return False
    
    first = token.span
    if lexer.try_eat(TokenKind.Mut, token):
        mut = True
    else:
        mut = False
    
    ident = lexer.expect(TokenKind.Ident, "variable name after 'let'").text
    lexer.expect(TokenKind.Eq, "'=' after variable name")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '='")
    expr = ref.value
    span = first.until(expr.span)
    ref.value = Let(mut, ident, expr, span)
    return True


def parse_type(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    name = token.text
    start = token.span
    args = []
    if lexer.try_eat(TokenKind.BrackOpen, token):
        if not parse_type(lexer, ref):
            fail_expect("type after '['")
        args.append(ref.value)
        while True:
            if not lexer.try_eat(TokenKind.Comma, token):
                break
            elif not parse_type(lexer, ref):
                break
            args.append(ref.value)
        end = lexer.expect(TokenKind.BrackClose, "closing ']'")
        span = start.until(end.span)
    else:
        span = start
    
    ref.value = Type(name, args, span)
    return True


def parse_field_assignment(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    field = token.text
    lexer.expect(TokenKind.Eq, "'=' after field name")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '='")
    expr = ref.value
    ref.value = (field, expr)
    return True


def parse_new(lexer, ref):
    if not lexer.try_eat(TokenKind.New, token):
        return False
    
    first = token.span
    if not parse_type(lexer, ref):
        lexer.fail_expect("type after 'new'")
    
    typ = ref.value
    
    if lexer.try_eat(TokenKind.End, token):
        span = first.until(token.span)
        ref.value = New(typ, [], span)
        return True

    lexer.expect(TokenKind.Where, "'where' or 'end' after type")
    if not parse_field_assignment(lexer, ref):
        lexer.fail_expect("field assignment")
    
    assignments = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Newline, token):
            break
        if not parse_field_assignment(lexer, ref):
            break
        assignments.append(ref.value)
    
    end = lexer.expect(TokenKind.End, "'end' to end 'new'-expression")
    span = first.until(end.span)
    ref.value = New(typ, assignments, span)
    return True


def parse_pargroup(lexer, ref):
    if not lexer.try_eat(TokenKind.ParOpen, token):
        return False
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '('")
    expr = ref.value
    lexer.expect(TokenKind.ParClose, "closing ')' after expression")
    ref.value = expr
    return True


def parse_for(lexer, ref):
    if not lexer.try_eat(TokenKind.For, token):
        return False
    start = token.span
    var = lexer.expect(TokenKind.Ident, "var after 'for").text
    lexer.expect(TokenKind.In, "'in' after loop variable")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after 'in'")
    first = ref.value
    if lexer.try_eat(TokenKind.DotDot, token):
        if not parse_expr(lexer, ref):
            lexer.fail_expect("expression after '..'")
        second = ref.value
        iterable = Range(first, second, first.span.until(second.span))
    else:
        iterable = first
    lexer.expect(TokenKind.Do, "'do' after iterable expression")
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'for'-statement")
    span = start.until(end.span)
    ref.value = For(var, iterable, body, span)
    return True


def parse_while(lexer, ref):
    if not lexer.try_eat(TokenKind.While, token):
        return False
    start = token.span
    if not parse_expr(lexer, ref):
        lexer.fail_expect("condition expression after 'while'")
    cond = ref.value
    lexer.expect(TokenKind.Do, "'do' after condition")
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'while'-statement")
    span = start.until(end.span)
    ref.value = While(cond, body, span)
    return True


def parse_do(lexer, ref):
    if not lexer.try_eat(TokenKind.Do, token):
        return False
    start = token.span
    exprs = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'do'-expression")
    span = start.until(end.span)
    ref.value = Do(exprs, span)
    return True


def parse_nil(lexer, ref):
    if lexer.try_eat(TokenKind.Nil, token):
        ref.value = Nil(token.span)
        return True
    else:
        return False


def parse_panic(lexer, ref):
    if not lexer.try_eat(TokenKind.Panic, token):
        return False
    start = token.span
    lexer.expect(TokenKind.ParOpen, "'(' after 'panic'")
    message = lexer.expect(TokenKind.Ident, "identifier as panic message")
    end = lexer.expect(TokenKind.ParClose, "')' after panic message")
    span = start.until(end.span)
    ref.value = Panic(message.text, span)
    return True


def parse_atom(lexer, ref):
    for atom_parser in ATOMS:
        if atom_parser(lexer, ref):
            return True
    
    return False


UNOPS = {TokenKind.Minus, TokenKind.Plus, TokenKind.Not}
def parse_unary_ops(lexer):
    ops = []
    while True:
        if lexer.peek().kind in UNOPS:
            ops.append(lexer.next().text)
        else:
            break
    
    return ops


def parse_single(lexer, ref):
    #print("Parsing single...")
    ops = parse_unary_ops(lexer)
    #print("  Unops: {}".format(ops))
    if not parse_atom(lexer, ref):
        if ops:
            lexer.fail_expect("expression after unary operators")
        else:
            return False
    
    expr = ref.value

    # Parse trailing attributes
    while True:
        # Call
        if lexer.try_eat(TokenKind.ParOpen, token):
            args = []
            while True:
                if not parse_expr(lexer, ref):
                    break
                args.append(ref.value)
                if not lexer.try_eat(TokenKind.Comma, token):
                    break

            end = lexer.expect(TokenKind.ParClose, token)
            span = expr.span.until(end.span)
            expr = Call(expr, args, span)
        
        # Member access
        elif lexer.try_eat(TokenKind.Dot, token):
            if not lexer.try_eat(TokenKind.Ident, token):
                lexer.fail_expect("field name after '.'")
            field = token.text
            span = expr.span.until(token.span)
            expr = Member(expr, field, span)
        
        # Indexing
        elif lexer.try_eat(TokenKind.BrackOpen, token):
            if not parse_expr(lexer, ref):
                lexer.fail_expect("index expression after '['")
            index = ref.value
            end = lexer.expect(TokenKind.BrackClose, "closing ']' after index expression")
            span = expr.span.until(end.span)
            expr = Indexing(expr, index, span)

        else:
            break

    
    # Apply unary operators after the trail modifiers
    ops.reverse()
    for op in ops:
        expr = Op(op, [expr], expr.span) # bad span usage
    
    #print("  -> {}".format(expr))
    ref.value = expr
    return True


BINOPS = {
    TokenKind.Plus, TokenKind.Minus, TokenKind.Slash, TokenKind.Star,
    TokenKind.Lt, TokenKind.LtEq, TokenKind.Gt, TokenKind.GtEq, TokenKind.EqEq, TokenKind.NotEq,
    TokenKind.And, TokenKind.Or,
}
def parse_binop(lexer, ref):
    if lexer.peek().kind in BINOPS:
        ref.value = lexer.next().text
        return True

    return False


ASSOPS = {
    TokenKind.Eq, TokenKind.PlusEq, TokenKind.MinusEq, 
    TokenKind.SlashEq, TokenKind.StarEq, TokenKind.PercentEq,
}
def parse_assop(lexer, ref):
    if lexer.peek().kind in ASSOPS:
        ref.value = lexer.next().text
        return True
    
    return False


def parse_expr(lexer, ref):
    if not parse_single(lexer, ref):
        return False
    
    #print("Parsing expr...")

    first = ref.value
    #print("  First: {}".format(first))

    if parse_assop(lexer, ref):
        #print("  Assignment!")
        loc = first
        op = ref.value
        if not parse_expr(lexer, ref):
            lexer.fail_expect("expression")
        
        expr = ref.value
        ref.value = Assign(loc, op, expr, first.span.until(expr.span))
        return True
    
    parts = [first]
    while True:
        if not parse_binop(lexer, ref):
            break
        
        #print("  Binary! ({})".format(ref.value))

        parts.append(ref.value)
        if not parse_single(lexer, ref):
            lexer.fail_expect("expression")
        
        parts.append(ref.value)

    expr = order_binary_expr(parts)
    ref.value = expr
    #print("  Done!")
    return True


def parse_exprs(lexer):
    exprs = []
    while True:
        while lexer.try_eat(TokenKind.Newline, token):
            continue
        
        if parse_expr(lexer, ref):
            exprs.append(ref.value)
            if not lexer.try_eat(TokenKind.Newline, token):
                break
        else:
            break
    
    return exprs


def parse_typed_ident(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    ident = token.text
    lexer.expect(TokenKind.Colon, "':' after identifier")
    if not parse_type(lexer, ref):
        lexer.fail_expect("type after ':'")
    typ = ref.value
    ref.value = (ident, typ)
    return True


def parse_argdefs(lexer):
    if not parse_typed_ident(lexer, ref):
        return []
    
    argdefs = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Comma, token):
            break
        if not parse_typed_ident(lexer, ref):
            break
        argdefs.append(ref.value)
    
    return argdefs
    


def parse_fundec(lexer, ref):
    if not lexer.try_eat(TokenKind.Fn, token):
        return False
    
    start = token.span
    name = lexer.expect(TokenKind.Ident, "function name").text
    lexer.expect(TokenKind.ParOpen, "'('")
    argdefs = parse_argdefs(lexer)
    lexer.expect(TokenKind.ParClose, "')'")
    ret = None
    if not lexer.try_eat(TokenKind.Newline, token): # Check for newline first so it isn't eaten.
        if lexer.try_eat(TokenKind.Arrow, token):
            if not parse_type(lexer, ref):
                lexer.fail_expect("return type after '->'")
            ret = ref.value
            lexer.expect(TokenKind.Newline, "newline after function signature")
    
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to close function declaration")
    span = start.until(end.span)
    ref.value = FunDec(name, argdefs, ret, body, span)
    return True


def parse_fields(lexer):
    #print("Parsing fields...")
    if not parse_typed_ident(lexer, ref):
        #print("  Could not parse first typed ident")
        return []
    
    fields = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Newline, token):
            #print("  No newline separator")
            break
        if not parse_typed_ident(lexer, ref):
            #print("  No typed ident")
            break
        fields.append(ref.value)
    
    return fields
        


def parse_typedec(lexer, ref):
    if not lexer.try_eat(TokenKind.Type, token):
        return False
    
    start = token.span
    name = lexer.expect(TokenKind.Ident, "type name").text
    if lexer.try_eat(TokenKind.End, token):
        span = start.until(token.span)
        ref.value = TypeDec(name, [], span)
        return True
    
    lexer.expect(TokenKind.Of, "'of' or 'end' after type name")
    fields = parse_fields(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end type declaration")
    span = start.until(end.span)
    ref.value = TypeDec(name, fields, span)
    return True

ATOMS = [
    parse_num, parse_bool, parse_nil, parse_var, parse_if, parse_let, 
    parse_pargroup, parse_new, parse_for, parse_while, parse_do, parse_panic,
    parse_fundec, parse_typedec,
]


def parse_toplevel(lexer):
    items = []
    while True:
        if lexer.try_eat(TokenKind.Eof, token):
            break
        
        if parse_expr(lexer, ref):
            items.append(ref.value)
        
        else:
            lexer.fail_expect("function declaration")

    return items


def parse(text):
    lexer = Lexer(text)
    items = parse_toplevel(lexer)
    lexer.expect(TokenKind.Eof, "end-of-file")
    return items
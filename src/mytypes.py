# =========================== Type Definitions ===============================
"""

So, first the AST is parsed for type signatures, and these are recorded.

When the program has to be type-checked, the type-references from the AST should be mapped to type signatures, or an error should be reported if the type is not defined.

A type signature can then be used to instantiate types, that are typically kept in a set of types.... hm.

"""



from ast import ast_node, FunDec, TypeDec, Span
from ast import Type as AstType
from collections import namedtuple


def format_type_into(typ, seq):
    if type(typ) == TypeSet:
        mapped = [format_type(t) for t in typ.types]
        mapped.sort()
        if len(typ.types) > 1:
            seq.append("{")
        last = len(typ.types) - 1
        for i, t in enumerate(mapped):
            seq.append(t)
            if i != last:
                seq.append(", ")
        
        if len(typ.types) > 1:
            seq.append("}")
    
    elif type(typ) == Type:
        seq.append(typ.signature.name)
        if typ.generic_params:
            seq.append("[")
            last = len(typ.generic_params) - 1
            for i, gen in enumerate(typ.generic_params):
                format_type_into(gen, seq)
                if i != last:
                    seq.append(", ")
            seq.append("]")
    
    else:
        raise Exception("format_type_into: Unknown type: {}".format(type(typ)))


def format_type(typ):
    #print("Formatting type: {!r}".format(typ))
    seq = []
    format_type_into(typ, seq)
    return "".join(seq)


# The description of a particular type
TypeSig = ast_node("TypeSig", object, [
    ("name", str), 
    ("generic_count", int),
    ("fields", list),
])

# Signatures of the built-int types
Nil     = TypeSig("nil",        0, [])
Bool    = TypeSig("bool",       0, [])
Int     = TypeSig("int",        0, [])
Panic   = TypeSig("panic",      0, [])
Uninit  = TypeSig("uninit",     0, [])
Vec     = TypeSig("Vec",        1, [])
Generic = TypeSig("Generic",    0, [])

class SuperType:
    """A supertype for the Type AST node"""

# An instance of a particular type, with its generic arguments specified and 
# such
Type = ast_node("Type", SuperType, [
    ("signature", TypeSig), 
    ("generic_params", list),  # [Type]
])

# Simple singletons for simple types
TYPE_NIL        = Type(Nil,     [])
TYPE_BOOL       = Type(Bool,    [])
TYPE_INT        = Type(Int,     [])
TYPE_UNINIT     = Type(Uninit,  [])
TYPE_GENERIC    = Type(Generic, [])
TYPE_VEC        = Type(Vec,     [TYPE_GENERIC])
TYPE_PANIC      = Type(Panic,   [])

def can_be_used_as(own: Type, expected: Type):
    """Returns whether the 'own' type can be used as the expected type"""
    
    #print("{} can be used as {}: ".format(
    #    format_type(own), format_type(expected)
    #), end="")
    
    # If they point to the same object (most of the time this will be true)
    if own is expected:
        #print("True")
        return True

    # If the type is expected to be generic, it will always fit
    if expected.signature == Generic:
        #print("True")
        return True
    
    # If this is a panic type, it's valid everywhere
    if own.signature == Panic:
        #print("True")
        return True
    
    # If the signatures match, see if the generics are compatible
    if expected.signature == own.signature:
        for i, ownpar in enumerate(own.generic_params):
            expectedpar = expected.generic_params[i]
            if not can_be_used_as(ownpar, expectedpar):
                #print("False")
                return False
        
        #print("True")
        return True
    
    #print("False")
    return False


class TypeSet:
    """One or more possible types that an expression may have"""
    def __init__(self, typ):
        assert(type(typ) == Type)
        self.types = [typ]

    def add(self, t):
        """Adds a type to this type-set, returning whether the set was 
        updated or not"""
        assert(type(t) == type(self))
        updated = False
        add_uninit = t.contains(TYPESET_UNINIT)
        for typ in t.types:
            updated |= self._add(typ, add_uninit)
        return updated
    
    def _add(self, t: Type, add_uninit: bool):
        """Adds a type to this type-set, returning whether the set was
        updated or not. If 'add_uninit' parameter is not true, the
        Uninit type is removed from the set if found"""
        #print("Adding type {} to self...".format(t))
        assert(type(t) == Type)

        updated = False

        # Remove uninit if necessary
        if (not add_uninit) and (t != TYPE_UNINIT):
            for i, typ in enumerate(self.types):
                if typ == TYPE_UNINIT:
                    self.types.pop(i)
                    updated = True
                    break
        
        # Check if the type is already contained
        for typ in self.types:
            if t == typ:
                return updated
        
        # Add it if necessary
        self.types.append(t)
        return True
    
    def contains(self, typeset):
        """Returns whether the given type set is a subset of this set"""
        assert(type(typeset) == type(self))
        for typ in typeset.types:
            if not self.contains_type(typ):
                return False
        
        return True  
    
    def contains_type(self, checktyp):
        """Returns whether a particular Type is contained within this set"""
        assert(type(checktyp) == Type)
        for typ in self.types:
            if typ == checktyp:
                return True
        
        return False

    def __eq__(self, other):
        """Returns whether this type set is equivalent to another type set"""
        if type(other) != type(self):
            return False
        
        if len(other.types) != len(self.types):
            return False
        
        for i, owntyp in enumerate(self.types):
            othertyp = other.types[i]
            if othertyp != owntyp:
                return False
        
        return True
    
    def can_be_used_as(self, typeset):
        """Returns whether this type can be used as the type in the given 
        typeset"""
        assert(type(typeset) == type(self))
        # Currently: must be a perfect fit. Ideally this is where requirements
        # could be slacked so that if all contained types in the set
        # fulfil the expected interface there would be no problem.
        if len(self.types) != 1:
            return False
        
        if len(typeset.types) != 1:
            raise Exception("can_be_used_as: A typeset was expected to be more than one type!")
        
        exp = typeset.types[0]
        own = self.types[0]
        return can_be_used_as(own, exp)
        
    def __str__(self):
        return repr(self)
    
    def __repr__(self):
        #print("self.types: {}".format(self.types))
        return format_type(self)
    
    def __iter__(self):
        return iter(self.types)
    
    def __len__(self):
        return len(self.types)
    
    def clone(self):
        """Returns a shallow clone of this TypeSet (which should be what you 
        want)"""
        ts = TypeSet(TYPE_NIL)
        ts.types = [t for t in self.types]
        return ts


# Simple singletons for simple typesets
TYPESET_NIL     = TypeSet(TYPE_NIL)
TYPESET_BOOL    = TypeSet(TYPE_BOOL)
TYPESET_INT     = TypeSet(TYPE_INT)
TYPESET_UNINIT  = TypeSet(TYPE_UNINIT)
TYPESET_PANIC   = TypeSet(TYPE_PANIC)
TYPESET_VEC     = TypeSet(TYPE_VEC)
TYPESET_GENERIC = TypeSet(TYPE_GENERIC)


# ========================= Type-checking environment ==========================

class InvalidType(Exception):
    def __init__(self, errors, typ):
        self.errors = errors
        self.type = typ


class Env:
    """An environment providing look-up for function and type definitions"""
    def __init__(self, funsigs, funbodies, typesigs):
        self.funsigs = funsigs
        self.typesigs = typesigs
        self.funbodies = funbodies

    def get_type(self, ast_typ):
        """Returns a single type instance based on the given ast type, or 
        raises an error if the type is invalid"""
        typename = ast_typ.name
        if typename in self.typesigs:
            sig = self.typesigs[typename]
            args = []
            errors = []
            for ast_arg in ast_typ.args:
                try:
                    argtyp = self.get_type(ast_arg)
                except InvalidType as e:
                    errors.extend(e.errors)
                    args.append(e.typ)
            
            if len(ast_typ.args) != sig.generic_count:
                invalidsig = TypeSig(typename, len(ast_typ.args), sig.fields)
                invalidtyp = Type(invalidsig, args)
                errors.append(
                    "Type '{}' was given {} generic arguments instead of {}".format(
                    typename, len(ast_typ.args), sig.generic_count
                ))
                raise InvalidType(errors, invalidtyp)
            args = []
            for ast_arg in ast_typ.args:
                args.append(self.get_type(ast_arg))

            return Type(sig, args)
            
        else:
            invalidsig = TypeSig(typename, 0, [])
            invalidtyp = Type(invalidsig, [])
            raise InvalidType(["Reference to undeclared type: '{}'".format(typename)], invalidtyp)
    
    def get_type_set(self, ast_typ):
        """Returns a type set corresponding to the given type, or raises an error if the type is invalid"""
        typ = self.get_type(ast_typ)
        return TypeSet(typ)
        


FunSig = ast_node("FunSig", object, [
    ("argdefs", list),
    ("ret_type", object),
])

def parse_env(ast):
    """Parses the function and type definitions in a given environment"""
    funsigs = {}
    funbodies = {}
    typedecs = {}
    typesigs = {
        "nil": Nil,
        "bool": Bool,
        "int": Int,
        "Vec": Vec,            
    }
    errors = []

    # Collect the definitions and add errors if things are defined twice
    for expr in ast:
        if isinstance(expr, FunDec):
            if expr.ident in funsigs:
                errors.append("Function '{}' declared twice.")
                continue
            
            funsigs[expr.ident] = (expr.argdefs, expr.ret)
            funbodies[expr.ident] = expr.body

        elif isinstance(expr, TypeDec):
            if expr.ident in typedecs:
                errors.append("Type '{}' declared twice.")
                continue
            
            typedecs[expr.ident] = expr.fields
        
        else:
            pass    

    # Start by parsing the type signatures.
    for typ, fields in typedecs.items():
        # Create the type signature with no fields, so that it can be
        # referred to when checking fields
        typesigs[typ] = TypeSig(typ, 0, [])
    
    # Create an environment
    env = Env(funsigs, funbodies, typesigs)
    
    # Validate the fields of the types and create the right
    # Type instances for them
    for typ, fields in typedecs.items():
        checked_fields = []
        for fieldname, fieldtyp in fields:
            try:
                checked_typ = env.get_type_set(fieldtyp)
            except InvalidType as e:
                checked_typ = TYPESET_PANIC
                errors.append(e.description)
            
            checked_fields.append((fieldname, checked_typ))

        
        # Assign the updated fields
        env.typesigs[typ].fields = checked_fields
    
    # Do the same thing for the types in function signatures
    checked_funsigs = {}
    for name, (argdefs, ret) in funsigs.items():
        checked_argdefs = []
        for param, ast_typ in argdefs:
            try:
                checked_typ = env.get_type_set(ast_typ)
            except InvalidType as e:
                checked_typ = TYPESET_PANIC
                errors.append(e.description)
            
            checked_argdefs.append((param, checked_typ))
        
        if ret is None:
            checked_ret = TYPESET_NIL
        else:
            checked_ret = env.get_type_set(ret)
        
        checked_funsigs[name] = FunSig(checked_argdefs, checked_ret)
        
    env.funsigs = checked_funsigs

    return (env, errors)


def main():
    tint = TypeSet(TYPE_INT)
    tint2 = TypeSet(TYPE_INT)

    print("int as int: {}".format(tint2.can_be_used_as(tint)))

    tbool = TypeSet(TYPE_BOOL)
    tbool2 = TypeSet(TYPE_BOOL)
    
    print("bool as bool: {}".format(tbool2.can_be_used_as(tbool)))

    print("bool as int: {}".format(tbool.can_be_used_as(tint)))

    print("int as bool: {}".format(tint.can_be_used_as(tbool)))

    tvec = TypeSet(TYPE_VEC)
    tvecint = TypeSet(Type(Vec, [TYPE_INT]))

    print("Vec[int] as Vec[T]: {}".format(tvecint.can_be_used_as(tvec)))
    print("Vec[T] as Vec[int]: {}".format(tvec.can_be_used_as(tvecint)))
    print("int as Vec[T]: {}".format(tint.can_be_used_as(tvec)))

    tpanic = TypeSet(TYPE_PANIC)
    print("panic as int: {}".format(tpanic.can_be_used_as(tint)))
    print("panic as bool: {}".format(tpanic.can_be_used_as(tbool)))
    print("panic as Vec[T]: {}".format(tpanic.can_be_used_as(tvec)))

    env, _errs = parse_env([])
    SPAN = Span(0, 0)
    int_ast = AstType("int", [], SPAN)
    vec_int_ast = AstType("Vec", [int_ast], SPAN)
    print("parsed vec int: {}".format(env.get_type_set(vec_int_ast)))
    print("parsed int: {}".format(env.get_type_set(int_ast)))

    tboolint = TypeSet(TYPE_BOOL)
    tboolint.add(TYPESET_INT)
    print("tboolint: {}".format(tboolint))
    print("{{bool, int}} contains int: {}".format(tboolint.contains(TypeSet(TYPE_INT))))
    print("{{bool, int}} contains bool: {}".format(tboolint.contains(TypeSet(TYPE_BOOL))))


if __name__ == "__main__":
    main()


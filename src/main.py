# The main module for the bachelor tan compiler

"""
Notes: use the 'immutables' Python library for HAMTs to represent state in the 
program analysis part.

"""

from lexer import Lexer, TokenKind
from parser import parse
from ast2ba import create_program_graph, format_ba
from pprint import pprint
from binder import bind_var_names
from analysis import type_check
from util import graph_to_type_change_png, graph_to_png
from mytypes import parse_env
from mywarnings import collect_warnings, format_warning
from codegen import codegen_sym_instrs, format_instr

import sys
import tempfile
import os
import subprocess


def main():
    args = sys.argv[1:]
    if len(args) < 1:
        print("Usage: python3 simpleparse.py <tanfile>")
        sys.exit(1)

    with open(args[0]) as f:
        source = f.read()
    
    lexer = Lexer(source)
    print("Tokens:")
    while True:
        token = lexer.next()
        text = token.text if token.kind != TokenKind.Newline else '\\n'
        print(f"  '{text}' ({token.kind}) [{token.span}]")
        if token.kind == TokenKind.Eof:
            break
    print("")

    ast = parse(source)

    print("Top-level items:")
    for expr in ast:
        print(f"  {expr.fmt(indent=1)}")

    print("Binding variables...")
    ast = bind_var_names(ast)

    print("")
    print("Converting to Basic Actions:")
    graph = create_program_graph(ast)
    
    print("Graph:")
    print("Edges:")
    pprint(graph.edges)
    dot = graph.to_dot(lambda ba, _s, _d: format_ba(ba))
    print("Actions:")
    print(dot)

    print("Creating PNG...")
    file_head = os.path.basename(args[0]).rsplit(".", 1)[0]
    graph_to_png(graph, file_head + ".png")
    print("")

    env, errors = parse_env(ast)
    if errors:
        print("Env parsing errors:")
        for err in errors:
            print("  {}".format(err))
    
    domain = type_check(graph, env)

    print("Creating checked PNG...")
    graph_to_type_change_png(graph, domain, file_head + "_tc.png")
    print("")

    print("Collecting warnings...")
    warnings = collect_warnings(graph, domain, env)
    if warnings:
        print("Warnings:")
        for warning in warnings:
            #print("  {}:{}: '{}'".format(warning.span.start, warning.span.end, warning.message))
            print(format_warning(warning, source))
            print("")
    else:
        print("No warnings found!")

    sym_instrs = codegen_sym_instrs(graph, domain)
    print("Symbolic instructions:")
    for instr in sym_instrs:
        print(format_instr(instr))

    """print("Final type assignments:")
    vertex_names = graph.get_vertex_names()
    bfs_order = {k: int(v) for k, v in vertex_names.items() if k != 0 and k != 1}
    bfs_order[0] = 0
    bfs_order[1] = graph.next_vertex_id
    for k, assignments in sorted(domain.items(), key = lambda it: bfs_order[it[0]]):
        print("{}:".format(vertex_names[k]))
        print("  {", end="")
        for var, typ in sorted(assignments.items()):
            print("{}: {}, ".format(var, typ), end="")
        print("}")"""
    
    
    


if __name__ == "__main__":
    main()
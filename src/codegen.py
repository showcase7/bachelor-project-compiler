

from ast2ba import *
from ast import ast_node
from vm import *
from mytypes import *


class SymOp:
    pass

Label = ast_node("Label", SymOp, [
    ("id", int)
])
SymBnz = ast_node("SymBnz", SymOp, [
    ("label", Label),
])
SymBez = ast_node("SymBez", SymOp, [
    ("label", Label),
])
SymJump = ast_node("SymJump", SymOp, [
    ("label", Label),
])


NEXT_LABEL_ID = 1
def gen_label():
    global NEXT_LABEL_ID
    lab_id = NEXT_LABEL_ID
    NEXT_LABEL_ID += 1
    return Label(lab_id)

"""BNum = ast_node("BNum", AtomicValue, [
    ("value", int), 
    ("span", Span)
])
BBool = ast_node("BBool", AtomicValue, [
    ("value", bool), 
    ("span", Span),
])
BNil = ast_node("BNil", AtomicValue, [
    ("span", Span),
])
BVar = ast_node("BVar", AtomicValue, [
    ("ident", str),
    ("span", Span),
])
BMember = ast_node("BMember", AtomicValue, [
    ("expr", AtomicValue), 
    ("member", str), 
    ("span", Span),
])
BUn = ast_node("BUn", CompoundValue, [
    ("op", str),
    ("value", AtomicValue), 
    ("span", Span),
])
BBin = ast_node("BBin", CompoundValue, [
    ("op", str),
    ("left", AtomicValue), 
    ("right", AtomicValue), 
    ("span", Span),
])
BAssign = ast_node("BAssign", BasicAction, [
    ("loc", AtomicValue), 
    ("value", CompoundValue), 
    ("span", Span),
])
BLet = ast_node("BLet", BasicAction, [
    ("is_mut", bool), 
    ("ident", str),
    ("value", object), 
    ("span", Span),
])
BPushScope = ast_node("BPushScope", AtomicValue, [
])
BPopScope = ast_node("BPopScope", AtomicValue, [
])
BTest = ast_node("BTest", AtomicValue, [
    ("value", CompoundValue), 
    ("span", Span),
])
BElse = ast_node("BElse", AtomicValue, [
    ("span", Span),
])
BNew = ast_node("BNew", AtomicValue, [
    ("type", Type), 
    ("span", Span),
])
BCall = ast_node("BCall", AtomicValue, [
    ("ident", AtomicValue), 
    ("args", list), 
    ("span", Span ),
])
BIndexing = ast_node("BIndexing", AtomicValue, [
    ("value", AtomicValue), 
    ("index", AtomicValue), 
    ("span", Span ),
])
BPanic = ast_node("BPanic", AtomicValue, [
    ("message", str), 
    ("span", Span ),
])
BTypeAssert = ast_node("BTypeAssert", AtomicValue, [
    ("value", AtomicValue),
    ("type", object), # TypeSet
    ("span", Span),
])"""

def generates_value(ba: BasicAction):
    if isinstance(ba, BPushScope):
        return False
    elif isinstance(ba, BPopScope):
        return False
    elif isinstance(ba, BElse):
        return False
    elif isinstance(ba, BAssign):
        return False
    elif isinstance(ba, BLet):
        return False
    else:
        return True


BINOP_INSTRS = {
    "+": Add(),
    "-": Sub(),
    "/": Div(),
    "*": Mul(),
    "%": Mod(),
    "<": Lt(),
    "<=": LtEq(),
    ">": Gt(),
    ">=": GtEq(),
    "==": Eq(),
    "!=": Neq(),
}
UNOP_INSTRS = {
    "!": Not(),
    "not": Not(),
    "-": Neg(),
    "+": Nop(),
}
NIL_VALUE = -1337


def size_of_typ(typ: Type):
    """Returns the size of a value of the given type"""
    # TODO: Since user-defined types aren't supported yet, this is still true...
    return 1


def size_of(typeset: TypeSet):
    """Returns the size of a value whose types are in the given type set"""
    if len(typeset) == 1:
        return size_of_typ(typeset.types[0])
    else:
        # The tag plus the largest contained type
        return 1 + max(size_of_typ(typ) for typ in typeset)
    


class Codegen:
    """Code generator object"""
    def __init__(self, var_types, types, domain):
        # The greatest typeset for each variable
        # Should probably be replaced by a size indicator or something
        self.var_types = var_types

        # The list of all types in the program, used for tagging values
        self.types = types

        # The types of variables at each point in the program graph
        self.domain = domain

        # The list of instructions being generated
        self.instrs = []

        # A map from a variable name to where in the stack it's located
        self.stack_index = {}

        # A list of the stack offsets at which the current scope starts. 
        # When the scope is popped, all variables above that should be freed.
        self.scope_starts = []

        # Where in the stack the next variable should be allocated
        self.stack_offset = 0
    
    def get_tag(self, typ: TypeSet):
        """Gets the tag representing the given type at run-time"""
        for i, owntyp in enumerate(self.types):
            if typ == owntyp:
                return i
        
        raise Exception("Type {} not found in type index!".format(typ))

    def get_size(self, ba: BasicAction, src: int):
        """Returns the allocation size of the given basic action"""
        if isinstance(ba, BVar):
            return size_of(self.domain[src][ba.ident])
        elif isinstance(ba, BTypeAssert):
            return size_of(ba.type)
        else:
            # TODO: support for user-defined types
            return 1
    
    def push_scope(self):
        """Pushes a new scope to the code generator"""
        self.scope_starts.append(self.stack_offset)
    
    def pop_scope(self):
        """Pops a scope, popping variables defined in this scope"""
        start = self.scope_starts.pop()
        diff = self.stack_offset - start
        self.stack_offset = start
        if diff:
            self.instrs.append(PopMany(diff))
    
    def save_scope(self):
        """Returns a copy of the current scope information"""
        print("Saving cg scope")
        return (self.stack_offset, [s for s in self.scope_starts])
    
    def restore_scope(self, scope):
        """Restores the scope"""
        print("Restoring cg scope")
        offset, starts = scope
        self.stack_offset = offset
        self.scope_starts = starts
    
    def reserve_stack_slot(self, size: int):
        """Reserves a spot on the stack for a value of the given size and returns a stack index to the slot"""
        offset = self.stack_offset
        self.stack_offset += size
        return offset
    
    def bind_variable(self, var: str, size: int):
        """Reserves a slot on the stack for the given variable"""
        offset = self.reserve_stack_slot(size)
        self.stack_index[var] = offset
    
    def append_instr(self, instr):
        """Appends an instruction to the list"""
        self.instrs.append(instr)
    
    def gen_toplevel(self, ba: BasicAction, jumplab: Label, src: int, dst: int):
        """Generates code for the given basic action and potentially removes
        the value it has generated on the stack, since it is a top-level
        expression"""
        self.gen(ba, jumplab, src, dst)
        if generates_value(ba):
            size = self.get_size(ba, src)
            self.instrs.append(PopMany(size))
        
        # Tag or untag values if necessary
        for var, srctyp in self.domain[src].items():
            srclen = len(srctyp)
            dsttyp = self.domain[dst][var]
            dstlen = len(dsttyp)
            # If the type is being widened to a union: tag it
            if srclen == 1 and dstlen > 1:
                # I really hope that this is correct enough wrt basic actions
                index = self.stack_index[var]
                tag = self.get_tag(srctyp)
                print("Tagging var '{}' with {}".format(var, tag))
                self.instrs.append(TagVar(index, tag))
            
            # If the type is being reduced again: untag it
            elif srclen > 1 and dstlen == 1:
                print("Untagging var '{}'".format(var))
                index = self.stack_index[var]
                self.instrs.append(UntagVar(index))

            else:
                pass

    def gen(self, ba: BasicAction, jumplab: Label, src: int, dst: int):
        """Generates program code for the given basic action into the
        internal instruction list of this code genreator"""

        if isinstance(ba, BNum):
            self.instrs.append(Num(ba.value))

        elif isinstance(ba, BBool):
            if ba.value:
                self.instrs.append(Num(1))
            else:
                self.instrs.append(Num(0))
        
        elif isinstance(ba, BNil):
            self.instrs.append(Num(NIL_VALUE))
        
        elif isinstance(ba, BUn):
            self.gen(ba.value, jumplab, src, dst)
            self.instrs.append(UNOP_INSTRS[ba.op])
        
        elif isinstance(ba, BLet):
            # Reserve a spot on the stack for this variable
            size = self.get_size(ba.value, src)
            self.bind_variable(ba.ident, size)

            # Generate the actual value
            if ba.value is not None:
                self.gen(ba.value, jumplab, src, dst)
            else:
                self.instrs.append(Num(NIL_VALUE))

        elif isinstance(ba, BVar):
            index = self.stack_index[ba.ident]
            size = self.get_size(ba, src)
            self.instrs.append(Num(index))
            self.instrs.append(Load(size))
        
        elif isinstance(ba, BAssign):
            # Generate the address
            size = self.get_size(ba, src)
            if isinstance(ba.loc, BVar):
                index = self.stack_index[ba.loc.ident]
                self.instrs.append(Num(index))
            elif isinstance(ba.loc, BIndexing):
                raise Exception("cg: no indexing support yet")
            elif isinstance(ba.loc, BMember):
                raise Exception("cg: no member support yet")

            # Generate the value
            self.gen(ba.value, jumplab, src, dst)

            # Store!
            self.instrs.append(Store)

        elif isinstance(ba, BPushScope):
            self.push_scope()
        
        elif isinstance(ba, BPopScope):
            self.pop_scope()
        
        elif isinstance(ba, BBin):
            if ba.op == "or" or ba.op == "||":
                pass
            elif ba.op == "and" or ba.op == "&&":
                pass
            else:
                self.gen(ba.left, jumplab, src, dst)
                self.gen(ba.right, jumplab, src, dst)
                self.instrs.append(BINOP_INSTRS[ba.op])
        
        elif isinstance(ba, BTest):
            self.gen(ba.value, jumplab, src, dst)
            self.instrs.append(SymBnz(jumplab))
        
        elif isinstance(ba, BElse):
            # Do nothing. The else branch is handled by the graph traverser
            pass 
        
        elif isinstance(ba, BTypeAssert):
            # Create the value and get its tag
            offset = self.stack_offset
            self.gen(ba.value, jumplab, src, dst)

            self.instrs.append(Num(offset))
            self.instrs.append(Load(1))

            # Duplicate the tag
            self.instrs.append(Dup())

            # Compare it with the expected tag
            tag = self.get_tag(ba.type)
            ok_lab = gen_label()
            self.instrs.append(Num(tag))
            self.instrs.append(Eq())
            self.instrs.append(SymBnz(ok_lab))

            # Gen individualized error code for the other cases or something
            message = "The type was not the expected type, {}".format(format_type(ba.type))
            self.instrs.append(IPanic(message))

            # OK case
            self.instrs.append(ok_lab)
            # Pop the extra value tag
            self.instrs.append(Pop())

            # Should i untag here instead/too? (yes)
            size = size_of(ba.type)
            self.instrs.append(Num(offset))
            self.instrs.append(Store(size))
            self.instrs.append(Pop())
    
        elif isinstance(ba, BPanic):
            self.instrs.append(IPanic(ba.message))

        else:
            raise Exception("Codegen.gen: Unhandled Basic Action: {}".format(type(ba).typename))


def collect_size_info(graph: Prograph, domain: dict):
    """Collects info about the types that each variable needs to hold for
    size allocation purposes. Additionally associates each type instance in 
    use with a particular tag used at runtime to discriminate this type in a
    context where a tagged value is used"""

    """
    This algorithm is horribly inefficient :)

    O(n * m * k^2), where n is the number of vertices in the program graph,
    m is the number of variables in the program
    and k is the number of distinct parameterized types in the program
    """

    types = {} # var: TypeSet
    # A list of all types found in the program, with the type's tag 
    # corresponding to its index in this list.
    found_types = [
        TYPESET_BOOL, 
        TYPESET_INT,
        TYPESET_NIL,
        TYPESET_PANIC,
        TYPESET_UNINIT,
    ] 
    i = 0
    for vertex, typemap in domain.items():
        for var, typeset in typemap.items():
            if not var in types:
                types[var] = typeset.clone()
            else:
                types[var].add(typeset)
                i += len(typeset.types) * len(typeset.types)
            
            # Go through the typeset
            for subtyp in typeset.types:
                is_new = True
                for ft in found_types:
                    i += 1
                    if ft.contains_type(subtyp):
                        is_new = False
                        break
                
                if is_new:
                    found_types.append(TypeSet(subtyp))
    
    # TODO: I should also sort the found types, so that the generated code is
    # not different based on what order the types were added (since that is)
    # what their runtime tag is

    upper_bound = len(domain) * len(types) * len(found_types) * len(found_types)
    print("collect_size_info: i = {} (upper bound: {})".format(i, upper_bound))
    return (types, found_types)



def is_join(graph: Prograph, vertex: int):
    if vertex == Prograph.START:
        return False
    return len(graph.rev[vertex]) > 1


def is_fork(graph: Prograph, vertex: int):
    if vertex == Prograph.END:
        return False
    return len(graph.edges[vertex]) > 1


def format_instr(instr: object):
    if isinstance(instr, Label):
        return "{}:".format(instr.id)
    elif isinstance(instr, SymJump):
        return "  Jump {}".format(instr.label.id)
    elif isinstance(instr, SymBnz):
        return "  Bnz {}".format(instr.label.id)
    elif isinstance(instr, SymBez):
        return "  Bez {}".format(instr.label.id)
    else:
        return "  {}".format(instr)
    #if isinstance(instr, )



def cg_seq_until_join_into(cg: Codegen, graph: Prograph, start: int, jump_addrs: dict, depth=0):
    """Generates instructions until the next join in the graph. Returns the following node, if any"""
    if depth > 5:
        raise Exception("Depth > 5")
    vertex_names = graph.get_vertex_names()
    src = start
    pad = depth * 2 * " "
    while src is not None:
        print("{}cg vertex {}".format(pad, vertex_names[src]))

        # This is an if-statement or while-statement pattern
        if is_fork(graph, src):
            print("{}  found fork".format(pad))
            # Unwrap the test and its destinations
            for dst, label in graph.edges[src].items():
                if isinstance(label, BElse):
                    elsevert = dst
                else:
                    ifvert = dst
                    testba = label
            
            # Generate some code for it to work
            start_scope = cg.save_scope()
            iflab = gen_label()
            elselab = gen_label()
            endlab = gen_label()
            cg.gen_toplevel(testba, iflab, src, dst)
            cg.append_instr(SymJump(elselab))

            # Generate the 'true' body
            print("{}  cg ifbody:".format(pad))
            cg.append_instr(iflab)
            ifend = cg_seq_until_join_into(cg, graph, ifvert, jump_addrs, depth=depth+1)
            ifscope = cg.save_scope()
            print("{}  ifend: {}".format(pad, vertex_names.get(ifend)))

            # Make sure to go the right place if it joins back in
            if ifend is not None:
                cg.append_instr(SymJump(endlab))
            
            # Generate the 'false' body
            print("{}  cg elsebody:".format(pad))
            cg.append_instr(elselab)
            cg.restore_scope(start_scope)
            elseend = cg_seq_until_join_into(cg, graph, elsevert, jump_addrs, depth=depth+1)

            # If this branch merges into the 'END' vertex
            if elseend is None:
                cg.append_instr(Terminate())
            
            cg.append_instr(endlab)
            
            # Continue as usual
            print("{}  checking join of if and else branches".format(pad))
            if ifend == elseend:
                next_src = elseend
                if next_src is not None:
                    print("{}  joined at {}".format(pad, vertex_names[elseend]))
                else:
                    print("{}  joined at END".format(pad))
            else:
                if elseend == None:
                    print("{}  elsepath terminated at END".format(pad))
                    cg.restore_scope(ifscope) # Resume at end of ifbody
                    next_src = ifend
                elif ifend == None:
                    print("{}  ifpath terminated at end".format(pad))
                    next_src = elseend
                else:
                    raise Exception("AMBIGUOUS JOIN!")
        
        # If this is not a fork
        else:
            if not src in graph.edges:
                return None
            
            # There is always just one edge here (not is_fork())
            for dst, label in graph.edges[src].items():
                cg.gen_toplevel(label, None, src, dst)
                next_src = dst
        
        if next_src is None:
            return None
        
        if next_src in jump_addrs:
            print("{}  path joined older vertex".format(pad))
            cg.append_instr(SymJump(jump_addrs[next_src]))
            #return next_src
            return None

        # If the 'next' vertex is a join, mark it or abort
        if is_join(graph, next_src):
            print("{}  found join".format(pad))
            return next_src 
        
        src = next_src
    
    return src


def cg_seq_into(cg: Codegen, graph: Prograph, start: int, jump_addrs: dict):
    """Generates code for the given program graph using the given code
    generator object, starting at the given vertex."""
    vertex_names = graph.get_vertex_names()
    src = start
    while src is not None:
        print("cgseq: {}".format(vertex_names[src]))

        # Codegen the sequence until it wants to join itself earlier
        new_src = cg_seq_until_join_into(cg, graph, src, jump_addrs)
        if new_src == src:
            print("Oh no: the new src was the old src :c!")
            break
        
        if new_src == None:
            break
        
        # Mark all joins with labels so that they are reachable from instructions
        join_lab = gen_label()
        jump_addrs[new_src] = join_lab
        cg.append_instr(join_lab)
        print("Generated label {} for vertex {}".format(join_lab.id, vertex_names[new_src]))

        src = new_src
    



def codegen_sym_instrs(graph: Prograph, domain: dict):
    """Generates a list of instructions for the given program graph, in
    which jumps are symbolic rather than fixed."""
    jump_addrs = {}
    var_types, found_types = collect_size_info(graph, domain)
    cg = Codegen(var_types, found_types, domain)
    cg_seq_into(cg, graph, Prograph.START, jump_addrs)
    return cg.instrs




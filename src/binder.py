"""
Renames variable names to a unique name based on their scoping
"""

from ast import *
from collections import deque, namedtuple
from pprint import pprint

Replaced = namedtuple("Replaced", ["old_bound_name"])
Added = namedtuple("Added", [])


class Binder:
    def __init__(self):
        # Maps from a variable name to a unique name for the variable based on
        # the scope it was declared in (for the current environment).
        self.bound_names = {} # str -> str

        # var_name:str -> int
        self.var_count = {}

        # Changes made to the environment in the current scope
        self.scope_changes = [{}]

        self.changes = self.scope_changes[-1]
    
    def bind_var(self, var):
        id_no = self.var_count.get(var, 1)
        self.var_count[var] = id_no + 1
        if not var in self.changes:
            if not var in self.bound_names:
                change = Added()
            else:
                change = Replaced(self.bound_names[var])
            self.changes[var] = change
        
        bound_name = "{}_{}".format(var, id_no)
        self.bound_names[var] = bound_name
        return bound_name
    
    def push_scope(self):
        self.scope_changes.append(self.changes)
        self.changes = {}
    
    def pop_scope(self):
        #print("Scope before pop:")
        pprint(self.bound_names)

        for var, change in self.changes.items():
            if isinstance(change, Replaced):
                self.bound_names[var] = change.old_bound_name
            elif isinstance(change, Added):
                self.bound_names.pop(var)
        
        self.changes = self.scope_changes.pop()
        #print("Scope after pop:")
        pprint(self.bound_names)

    def bind_vars_in_expr(self, expr):
        if isinstance(expr, Num): pass
        elif isinstance(expr, Bool): pass
        elif isinstance(expr, Nil): pass
        elif isinstance(expr, Panic): pass
        elif isinstance(expr, Type): pass
        elif isinstance(expr, TypeDec): pass
        elif isinstance(expr, Let):
            # Rebind the right hand side in the old environment
            if expr.expr is not None:
                expr.expr = self.bind_vars_in_expr(expr.expr)

            # Update the environment with this new variable instance
            var = expr.ident
            bound_name = self.bind_var(var)
            expr.ident = bound_name
            
        elif isinstance(expr, Var):
            var = expr.ident
            if var in self.bound_names:
                expr.ident = self.bound_names[var]
            else:
                return Panic("Reference to unbound variable '{}'".format(var), expr.span)
        
        elif isinstance(expr, Op):
            expr.args = [self.bind_vars_in_expr(arg) for arg in expr.args]
        
        elif isinstance(expr, Member):
            expr.object = self.bind_vars_in_expr(expr.object)

        elif isinstance(expr, Indexing):
            expr.target = self.bind_vars_in_expr(expr.target)
            expr.index = self.bind_vars_in_expr(expr.index)
        
        elif isinstance(expr, Call):
            # Don't bind function calls right now.
            #expr.callee = self.bind_vars_in_expr(expr.callee)
            expr.args = [self.bind_vars_in_expr(arg) for arg in expr.args]
        
        elif isinstance(expr, Assign):
            expr.loc = self.bind_vars_in_expr(expr.loc)
            expr.value = self.bind_vars_in_expr(expr.value)
        
        elif isinstance(expr, While):
            expr.cond = self.bind_vars_in_expr(expr.cond)
            self.push_scope()
            expr.body = self.bind_vars_in_exprs(expr.body)
            self.pop_scope()
        
        elif isinstance(expr, If):
            expr.cond = self.bind_vars_in_expr(expr.cond)
            self.push_scope()
            expr.ifbody = self.bind_vars_in_exprs(expr.ifbody)
            self.pop_scope()
            self.push_scope()
            expr.elsebody = self.bind_vars_in_exprs(expr.elsebody)
            self.pop_scope()
        
        elif isinstance(expr, For):
            self.push_scope()
            expr.iterable = self.bind_vars_in_expr(expr.iterable)
            expr.var = self.bind_var(expr.var)
            expr.body = self.bind_vars_in_exprs(expr.body)
            self.pop_scope()
        
        elif isinstance(expr, Range):
            expr.start = self.bind_vars_in_expr(expr.start)
            expr.end = self.bind_vars_in_expr(expr.end)
        
        elif isinstance(expr, Do):
            self.push_scope()
            expr.body = self.bind_vars_in_exprs(expr.body)
            self.pop_scope()
        
        elif isinstance(expr, New):
            # TODO: Consider whether assignments can refer to each other.
            expr.assignments = [
                (field, self.bind_vars_in_expr(ass)) for field, ass in expr.assignments
            ]
        
        elif isinstance(expr, FunDec):
            # TODO: Actually bind the vars inside of the function environment(empty, but with parameters bound)
            pass

        else:
            raise Exception("Unhandled expr type: {}".format(type(expr).typename))
        
        return expr

    def bind_vars_in_exprs(self, exprs):
        return [self.bind_vars_in_expr(expr) for expr in exprs]       
        


def bind_var_names(ast):
    binder = Binder()
    return binder.bind_vars_in_exprs(ast)

